import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {Storage} from '@ionic/storage';
import {AuthServiceProvider} from '../auth-service/auth-service';
import {Restangular} from 'ngx-restangular';
import {Geolocation} from '@ionic-native/geolocation';
import {UtilsProvider} from '../utils/utils';
import {Platform} from 'ionic-angular';

@Injectable()
export class InitServiceProvider {

    company_categories: Array<any>;
    regions: Array<any>;
    company: any;
    company_id: number = this.auth.company_id;
    coordinates: any = {lat: 0, lng: 0};
    companies: Array<any>;
    owner: boolean = false;
    approved: boolean = false;
    points: Array<any>;
    coupons: Array<any>;
    managers: Array<any>;
    countries: Array<{code: string, name: string}>;

    constructor(public restangular: Restangular,
                public storage: Storage,
                public auth: AuthServiceProvider,
                public geolocation: Geolocation,
                public utils: UtilsProvider,
                public platform: Platform) {
    }

    onInitApp() {

        return new Promise(resolve => {

            // this.getLocation().subscribe(data => console.log(data));

            if (this.auth.getToken() !== '') {

                this.company_id = this.auth.company_id;

                this.restangular.one('init', this.company_id).customGET('').subscribe(data => {

                    this.company_categories = data.company_categories;
                    this.regions = data.regions;
                    this.company = this.restangular.restangularizeElement('', data.company, 'companies');
                    this.points = data.points;
                    this.approved = this.company.approved;
                    this.managers = this.company.managers;
                    this.coupons = this.company.coupons;
                    this.countries = data.countries;

                    for (let manager of this.company.managers){

                        if (manager.id === this.auth.id && manager.owner === 1)
                            this.owner = true;
                        
                    }

                    resolve(data);


                });

            } else {
                resolve();
            }

        });

    }

}
