import {Injectable} from '@angular/core';
import {AlertController} from "ionic-angular";
import {Diagnostic} from '@ionic-native/diagnostic';
import {Observable} from 'rxjs/Observable';
import {BehaviorSubject} from "rxjs/BehaviorSubject";

@Injectable()
export class UtilsProvider {

    progress: number = 0;
    _progress: BehaviorSubject<number> = new BehaviorSubject(null);
    progress$: Observable<number> = this._progress.asObservable();

    uploadProcess: boolean = false;
    _uploadProcess: BehaviorSubject<boolean> = new BehaviorSubject(null);
    uploadProcess$: Observable<boolean> = this._uploadProcess.asObservable();

    constructor(public alertCtrl: AlertController,
                public diagnostic: Diagnostic) {}

    isAnyFieldEmpty(x) {

        let keys = Object.getOwnPropertyNames(x);

        for (let key in keys) {

            if (x[keys[key]] === '' || typeof x[keys[key]] === 'undefined' || x[keys[key]] === null) {

                let alert = this.alertCtrl.create({title: 'נא למלא את כל השדות', buttons: ['OK']});
                alert.present();

                return true;

            }

        }

        return false;

    }

    isLocationAvailable (): any {

        return new Observable(observer => {

            this.diagnostic.isLocationAuthorized().then(isAuthorized => {

                console.log('isLocationAuthorized', isAuthorized);

                if (!isAuthorized) {

                    this.diagnostic.requestLocationAuthorization().then(status => {

                        if (status === "GRANTED"){

                            this.diagnostic.isLocationEnabled().then(isEnabled => {

                                console.log('isLocationEnabled', isEnabled);

                                if (isEnabled){

                                    observer.next({status: 'success'});
                                    observer.complete();

                                } else {

                                    observer.next({status: 'error', message: "Location is disabled"});
                                    observer.complete();

                                }

                            })

                        } else {

                            observer.next({status: 'error', message: "Location is not authorized"});
                            observer.complete();

                        }

                    });

                } else {

                    this.diagnostic.isLocationEnabled().then(isEnabled => {

                        console.log('isLocationEnabled', isEnabled);

                        if (isEnabled){

                            observer.next({status: 'success'});
                            observer.complete();

                        } else {

                            observer.next({status: 'error', message: "Location is disabled"});
                            observer.complete();

                        }

                    })

                }

            })

        })

    }

    setProgress (value) {

        this.progress = value;
        this._progress.next(value);

    }

    setUploadProcess (value) {

        this.uploadProcess = value;
        this._uploadProcess.next(value);
    }

}
