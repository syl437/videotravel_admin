import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map';
import {BehaviorSubject, Observable} from "rxjs";
import {Storage} from "@ionic/storage";

@Injectable()
export class AuthServiceProvider {

    // Token as string

    public company_id: number;
    public id: number;

    public _token: BehaviorSubject<string> = new BehaviorSubject('');
    public readonly token$: Observable<string> = this._token.asObservable();
    public token: string;

    constructor(private storage: Storage) {
    }

    // Reads token from storage (should return promise)
    initialize() {
        return new Promise((resolve, reject) => {
            this.storage.get('token').then((token) => {

                this.storage.get('company_id').then((val) => {

                    this.storage.get('id').then((id) => {

                        this.id = id || null;
                        this.company_id = val || null;
                        this._token.next(token || '');
                        this.token = token || '';
                        resolve();

                    })

                });

            });
        });
    }

    // Getter
    getToken(): string {
        return this.token;
    }

    // Setter
    setToken(token: string): void {
        this.token = token;
        this.storage.set('token', this.token);
        this._token.next(token);
    }

}
