import {Component, ViewChild} from '@angular/core';
import {Events, Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {UtilsProvider} from "../providers/utils/utils";
import {LoginPage} from "../pages/login/login";
import {AuthServiceProvider} from "../providers/auth-service/auth-service";
import {CompanyProfilePage} from "../pages/company-profile/company-profile";
import {ProfilePage} from "../pages/profile/profile";
import {InitServiceProvider} from "../providers/init-service/init-service";
import {EmployeesPage} from "../pages/employees/employees";
import {CouponsPage} from "../pages/coupons/coupons";
import {ScanPage} from "../pages/scan/scan";
import {PointsPage} from "../pages/points/points";
import {ScreenOrientation} from "@ionic-native/screen-orientation";
import {Storage} from "@ionic/storage";
import {StatisticsPage} from '../pages/statistics/statistics';
import {SettingsPage} from '../pages/settings/settings';

@Component({
    templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any;
    pages: Array<{ title: string, component: any, icon: string }>;

    constructor(public platform: Platform,
                public statusBar: StatusBar,
                public splashScreen: SplashScreen,
                private auth: AuthServiceProvider,
                public utils: UtilsProvider,
                public init: InitServiceProvider,
                public screen: ScreenOrientation,
                public events: Events,
                public storage: Storage) {

        this.initializeApp();

        this.events.subscribe('login:success', () => {
            this.checkPages();
        });

        // used for an example of ngFor and navigation
        this.pages = [
            {title: 'עמוד אישי', component: ProfilePage, icon: 'person'},
            {title: 'סריקה', component: ScanPage, icon: 'qr-scanner'},
        ];

    }

    initializeApp() {

        this.platform.ready().then(() => {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            this.statusBar.styleDefault();
            this.splashScreen.hide();

            if (this.platform.is('cordova')){
                this.screen.lock('portrait');
            }

            this.auth.initialize().then(() => {

                if (this.auth.getToken() !== ''){

                    this.init.onInitApp().then(() => {

                        if (this.init.owner === true){
                            this.pages.push({title: 'הגדרות הודעות', component: SettingsPage, icon: 'chatboxes'});
                            this.pages.push({title: 'סטטיסטיקה', component: StatisticsPage, icon: 'stats'});
                            this.pages.push({title: 'עמוד העסק', component: CompanyProfilePage, icon: 'create'});
                            this.pages.push({title: 'עובדים', component: EmployeesPage, icon: 'people'});
                            this.pages.push({title: 'קופונים', component: CouponsPage, icon: 'cash'});

                        }

                        this.storage.get('hotel').then(hotel => {
                            if (hotel && hotel == 1){
                                this.pages.push({title: 'נקודות', component: PointsPage, icon: 'keypad'},);
                            }
                        });

                        this.rootPage = CompanyProfilePage;

                    })

                } else {
                    this.rootPage = LoginPage;
                }

            })
        });
    }

    checkPages( ) {
        if (this.init.owner === true){
            this.pages.push({title: 'הגדרות הודעות', component: SettingsPage, icon: 'chatboxes'});
            this.pages.push({title: 'סטטיסטיקה', component: StatisticsPage, icon: 'stats'});
            this.pages.push({title: 'עמוד העסק', component: CompanyProfilePage, icon: 'create'});
            this.pages.push({title: 'עובדים', component: EmployeesPage, icon: 'people'});
            this.pages.push({title: 'קופונים', component: CouponsPage, icon: 'cash'});

        }

        this.storage.get('hotel').then(hotel => {
            if (hotel && hotel == 1){
                this.pages.push({title: 'נקודות', component: PointsPage, icon: 'keypad'},);
            }
        });
    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }

    logout(): void {

        this.storage.clear().then(() => {

            this.auth.id = 0;
            this.auth.company_id = 0;
            this.auth.token = '';
            this.auth._token.next('');

            this.nav.setRoot(LoginPage);

        })

    }
}
