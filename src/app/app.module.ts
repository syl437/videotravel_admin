import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {AlertController, App, IonicApp, IonicErrorHandler, IonicModule, LoadingController} from 'ionic-angular';

import {MyApp} from './app.component';
import {LoginPage} from '../pages/login/login';
import {CompanyProfilePage} from '../pages/company-profile/company-profile';

import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {IonicStorageModule} from '@ionic/storage';
import {RestangularModule} from 'ngx-restangular';
import {Camera} from '@ionic-native/camera';
import {MediaCapture} from '@ionic-native/media-capture';
import {AgmCoreModule} from '@agm/core';
import {FileTransfer} from '@ionic-native/file-transfer';
import {Geolocation} from '@ionic-native/geolocation';

import {UtilsProvider} from '../providers/utils/utils';
import {AuthServiceProvider} from '../providers/auth-service/auth-service';
import {Diagnostic} from '@ionic-native/diagnostic';

import {InitServiceProvider} from '../providers/init-service/init-service';
import {AddPictureDirective} from '../directives/add-picture/add-picture';
import {AddVideoDirective} from '../directives/add-video/add-video';
import {NativeGeocoder} from '@ionic-native/native-geocoder';
import {VgCoreModule} from 'videogular2/core';
import {VgControlsModule} from 'videogular2/controls';
import {VgOverlayPlayModule} from 'videogular2/overlay-play';
import {VgBufferingModule} from 'videogular2/buffering';
import {CommonHeaderComponent} from '../components/common-header/common-header';
import {ProfilePage} from '../pages/profile/profile';
import {EmployeesPage} from '../pages/employees/employees';
import {CouponsPage} from '../pages/coupons/coupons';
import {AddCouponPage} from '../pages/add-coupon/add-coupon';
import {EditCouponPage} from '../pages/edit-coupon/edit-coupon';
import {ScanPage} from '../pages/scan/scan';
import {QRScanner} from '@ionic-native/qr-scanner';
import {PointsPage} from '../pages/points/points';
import {ScreenOrientation} from '@ionic-native/screen-orientation';
import {StatisticsPage} from '../pages/statistics/statistics';
import {SettingsPage} from '../pages/settings/settings';

export function RestangularConfigFactory(RestangularProvider, App, LoadingController, AlertController, AuthServiceProvider) {

    let loading = null;

    RestangularProvider.addFullRequestInterceptor((element, operation, path, url, headers, params)=> {

        loading = LoadingController.create({content: 'Please wait...'});
        loading.present();

        params.lang = 'he';
        return {params: params, headers: headers, element: element}
    });

    // RestangularProvider.setBaseUrl('http://vt.test/admin_api/v1');
    RestangularProvider.setBaseUrl('http://app.y-travel.net/admin_api/v1');
    // RestangularProvider.setBaseUrl('http://vt.kartisim.co.il/admin_api/v1');

    // Set initial default header

    RestangularProvider.setDefaultHeaders({Authorization: `Bearer ${AuthServiceProvider.getToken()}`});

    // Subscribe to token change

    AuthServiceProvider.token$.subscribe((token) => RestangularProvider.setDefaultHeaders({Authorization: `Bearer ${token}`}));

    RestangularProvider.addResponseInterceptor((data, operation, what, url, response) => {

        loading.dismiss();

        if (data.data) {
            console.log(url, data.data);
        }

        let extractedData = data.data;

        if (extractedData === null){
            extractedData = [];
        }

        return extractedData;

    });

    RestangularProvider.addErrorInterceptor((response, subject, responseHandler) => {

        loading.dismiss();

        console.log(response);

        if (response.error && response.error.message){
            AlertController.create({title:response.error.message, buttons: ['OK']}).present();
            return true;
        }

        if (response.data) {

            let title = response.data.message ? response.data.message : 'Server error';
            let message = '';

            if (response.data.error && response.data.error.message){
                message += response.data.error.message;
            }

            if (response.data.error && response.data.error.fields){
                for (let errors in response.data.error.fields){
                    for (let error in response.data.error.fields[errors]){
                        message += ' ' + response.data.error.fields[errors][error];
                    }
                }
            }

            AlertController.create({title:title, message: message, buttons: ['OK']}).present();

            if (response && response.data && response.data.status && response.data.status == 401){
                AuthServiceProvider.logout();
                App.getRootNavs()[0].setRoot('LoginPage');
            }

            return true;

        }

        return true;

    });

}

@NgModule({
    declarations: [
        MyApp,
        LoginPage,
        CompanyProfilePage,
        AddPictureDirective,
        AddVideoDirective,
        CommonHeaderComponent,
        ProfilePage,
        EmployeesPage,
        CouponsPage,
        AddCouponPage,
        EditCouponPage,
        ScanPage,
        PointsPage,
        StatisticsPage,
        SettingsPage
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        IonicStorageModule.forRoot({
            name: "__mydb",
            driverOrder: ["sqlite", "websql", "indexeddb"]
        }),
        RestangularModule.forRoot([App, LoadingController, AlertController, AuthServiceProvider], RestangularConfigFactory),
        AgmCoreModule.forRoot({apiKey: "AIzaSyA4uXPidpv7gDsUIXwS30CMQNs5M-t6DOs", libraries: ["places"]}),
        VgCoreModule,
        VgControlsModule,
        VgOverlayPlayModule,
        VgBufferingModule,
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        LoginPage,
        CompanyProfilePage,
        ProfilePage,
        EmployeesPage,
        CouponsPage,
        AddCouponPage,
        EditCouponPage,
        ScanPage,
        PointsPage,
        StatisticsPage,
        SettingsPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        {provide: ErrorHandler, useClass: IonicErrorHandler},
        Camera,
        MediaCapture,
        UtilsProvider,
        AuthServiceProvider,
        FileTransfer,
        Diagnostic,
        Geolocation,
        InitServiceProvider,
        NativeGeocoder,
        QRScanner,
        ScreenOrientation
    ]
})
export class AppModule {
}
