import {Component} from '@angular/core';
import {AlertController, Events, NavController} from 'ionic-angular';
import {Restangular} from 'ngx-restangular';
import {Storage} from '@ionic/storage';
import {CompanyProfilePage} from '../company-profile/company-profile';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';
import {InitServiceProvider} from '../../providers/init-service/init-service';

@Component({
    selector: 'page-company-login',
    templateUrl: 'login.html',
})

export class LoginPage {

    manager = {email: '', password: ''};
    // manager = {email: 'rona.tbf@gmail.com', password: 'aaayyyaaa'};

    constructor(public navCtrl: NavController,
                private restangular: Restangular,
                public alertCtrl: AlertController,
                private storage: Storage,
                public auth: AuthServiceProvider,
                public events: Events,
                public init: InitServiceProvider) {
    }

    async authenticate() {

        if (this.manager.email === '' || this.manager.password === '') {

            let alert = this.alertCtrl.create({title: 'נא למלא את כל השדות', buttons: ['OK']});
            alert.present();
            return;

        }

        this.restangular.all('tokens/managers').post(this.manager).subscribe(async (data) => {

            this.auth.setToken(data.api_token);

            await this.storage.set('id', data.id);
            await this.storage.set('company_id', data.company_id);
            await this.storage.set('hotel', data.is_hotel);

            this.auth.initialize().then(() => this.init.onInitApp().then(success => {

                    this.navCtrl.setRoot(CompanyProfilePage);
                    this.events.publish('login:success');

                })
            );

        });

    }

}
