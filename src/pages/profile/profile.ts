import {Component} from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';
import {UtilsProvider} from "../../providers/utils/utils";
import {Restangular} from "ngx-restangular";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {Storage} from "@ionic/storage";

@Component({
    selector: 'page-profile',
    templateUrl: 'profile.html',
})
export class ProfilePage {

    data: any = {name: '', old_password: '', new_password: ''};
    id: number = this.auth.id;

    constructor(public navCtrl: NavController,
                public utils: UtilsProvider,
                public restangular: Restangular,
                public alertCtrl: AlertController,
                public auth: AuthServiceProvider,
                public storage: Storage) {}

    update () {

        if (this.data.name === '' && (this.data.old_password === '' && this.data.new_password === '')){

            let alert = this.alertCtrl.create({title: 'Nothing to change', buttons: ['OK']});
            alert.present();

            return;

        }

        if (this.data.new_password !== '' && this.data.old_password === ''){

            let alert = this.alertCtrl.create({title: 'Please fill in old password', buttons: ['OK']});
            alert.present();

            return;

        }

        this.restangular.one('managers', this.id).patch(this.data).subscribe(data => {

            this.data.old_password = '';
            this.data.new_password = '';

            let alert = this.alertCtrl.create({title: 'Successfully changed', buttons: ['OK']});
            alert.present();

        });

    }

    ionViewWillEnter () {

        this.storage.get('id').then(val => {

            this.restangular.one('managers', val).get().subscribe(data => {

                this.data.name = data.name;

            });

        })

    }
}
