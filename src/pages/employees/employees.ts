import {Component} from '@angular/core';
import {AlertController} from 'ionic-angular';
import {InitServiceProvider} from '../../providers/init-service/init-service';
import {Restangular} from 'ngx-restangular';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';


@Component({
    selector: 'page-employees',
    templateUrl: 'employees.html',
})
export class EmployeesPage {

    employees = this.init.managers;

    constructor(
                public init: InitServiceProvider,
                public alertCtrl: AlertController,
                public restangular: Restangular,
                public auth: AuthServiceProvider) {}

    showPopup () {

        let that = this;

        let prompt = that.alertCtrl.create({
            title: 'New employee',
            message: "Enter a name and an email",
            inputs: [
                {
                    name: 'name',
                    placeholder: 'שם'
                },
                {
                    name: 'email',
                    placeholder: 'אימייל'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: data => {}
                },
                {
                    text: 'Send',
                    handler: data => {

                        if (data.email !== ''){

                            that.restangular.all('managers').post({name: data.name, email: data.email, company_id: that.auth.company_id})
                                .subscribe(data => {

                                    that.employees = data;
                                    that.init.managers = data;

                                })

                        }
                    }
                }
            ]
        });
        prompt.present();

    }

    deleteEmployee(id) {

        let confirm = this.alertCtrl.create({
            title: 'Delete employee?',
            buttons: [
                {
                    text: 'No',
                    handler: () => {}
                },
                {
                    text: 'Yes',
                    handler: () => {

                        this.restangular.one('managers', id).remove().subscribe(data => {

                            this.employees = data;
                            this.init.managers = data;

                        });

                    }
                }
            ]
        });

        confirm.present();

    }

}
