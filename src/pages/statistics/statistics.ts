import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import {Restangular} from 'ngx-restangular';
import {InitServiceProvider} from '../../providers/init-service/init-service';

@Component({
    selector: 'page-statistics',
    templateUrl: 'statistics.html',
})
export class StatisticsPage {

    company_id: number = this.init.company_id;
    codes;
    purchases;

    constructor(public navCtrl: NavController,
                public restangular: Restangular,
                public init: InitServiceProvider) {
    }

    async ionViewWillEnter() {
        try {
            let data = await this.restangular.one('companies', this.company_id).customGET('stats').toPromise();
            this.codes = data.codes;
            this.purchases = data.purchases;
        } catch (err) {
            console.log(err);
        }
    }

}
