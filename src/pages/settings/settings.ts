import {Component} from '@angular/core';
import {AlertController, NavController} from 'ionic-angular';
import {Restangular} from 'ngx-restangular';
import {AuthServiceProvider} from '../../providers/auth-service/auth-service';

@Component({
    selector: 'page-settings',
    templateUrl: 'settings.html',
})

export class SettingsPage {

    company;
    company_id = this.auth.company_id;

    constructor(public navCtrl: NavController,
                public auth: AuthServiceProvider,
                public alertCtrl: AlertController,
                public restangular: Restangular) {}

    async ionViewWillEnter() {
        try {
            this.company = await this.restangular.one('companies', this.company_id).customGET('admin').toPromise();
        } catch (err) {
            console.log(err);
        }
    }

    async update () {
        try {
            await this.restangular.one('companies', this.company_id).customPATCH({
                facebook_en: this.company.facebook_en,
                facebook_he: this.company.facebook_he,
                sms_en: this.company.sms_en,
                sms_he: this.company.sms_he,
            }, 'updateAdmin').toPromise();
            let alert = this.alertCtrl.create({title: 'Successfully updated', buttons: ['OK']});
            alert.present();
        } catch (err) {
            console.log(err);
        }
    }

}
