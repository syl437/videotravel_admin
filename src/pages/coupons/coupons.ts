import {Component} from '@angular/core';
import {AlertController, ModalController} from 'ionic-angular';
import {InitServiceProvider} from '../../providers/init-service/init-service';
import {Restangular} from 'ngx-restangular';
import {AddCouponPage} from '../add-coupon/add-coupon';
import {EditCouponPage} from '../edit-coupon/edit-coupon';

@Component({
    selector: 'page-coupons',
    templateUrl: 'coupons.html',
})
export class CouponsPage {

    coupons = this.init.coupons;

    constructor(public init: InitServiceProvider,
                public alertCtrl: AlertController,
                public restangular: Restangular,
                public modalCtrl: ModalController) {}

    deleteCoupon(id) {

        let confirm = this.alertCtrl.create({
            title: 'Delete coupon?',
            buttons: [
                {
                    text: 'No',
                    handler: () => {}
                },
                {
                    text: 'Yes',
                    handler: () => {

                        this.restangular.one('coupons', id).remove().subscribe(data => {

                            this.coupons = data;
                            this.init.coupons = data;

                        });

                    }
                }
            ]
        });

        confirm.present();

    }

    addCoupon () {

        let modal = this.modalCtrl.create(AddCouponPage);
        modal.present();

        modal.onDidDismiss(data => {

            if (typeof data !== 'undefined'){
                this.coupons = data;
                this.init.coupons = data;
            }

        });

    }

    editCoupon (coupon) {

        let modal = this.modalCtrl.create(EditCouponPage, {coupon_id: coupon});
        modal.present();

        modal.onDidDismiss(data => {

            if (typeof data !== 'undefined') {
                this.coupons = data;
                this.init.coupons = data;
            }

        });

    }

}
