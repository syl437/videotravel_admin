import {Component, NgZone} from '@angular/core';
import {AlertController} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {InitServiceProvider} from "../../providers/init-service/init-service";
import {Restangular} from "ngx-restangular";
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {MapsAPILoader} from "@agm/core";
import {UtilsProvider} from "../../providers/utils/utils";
import {} from '@types/googlemaps';

@Component({
    selector: 'page-company-profile',
    templateUrl: 'company-profile.html',
})

export class CompanyProfilePage {

    company;
    company_id: number = this.auth.company_id;
    company_categories: Array<object> = this.init.company_categories;
    regions: Array<object> = this.init.regions;
    formatted_address: Array<any>;
    newLogo: any;
    newVideo: any;
    src: string;
    localProgress: any = {logo: false, video: false};
    process: boolean = this.utils.uploadProcess;
    progress: number;

    constructor(public storage: Storage,
                public restangular: Restangular,
                public init: InitServiceProvider,
                public alertCtrl: AlertController,
                public transfer: FileTransfer,
                public auth: AuthServiceProvider,
                public loader: MapsAPILoader,
                public utils: UtilsProvider,
                private zone: NgZone) {

        this.utils.progress$.subscribe(val => {

            this.zone.run(() => {this.progress = val;});

        });

    }

    update () {

        console.log(this.company.plain());

        delete this.company.website;
        delete this.company.approved;
        delete this.company.from_user;
        delete this.company.pin;
        delete this.company.rate;
        delete this.company.stars;
        delete this.company.thumbnail;
        delete this.company.main_video;
        delete this.company.logo;
        delete this.company.translations;

        if (!this.utils.isAnyFieldEmpty(this.company.plain())){

            this.company.address = this.formatted_address[0];
            this.company.country = this.formatted_address[1];
            this.company.lat = this.formatted_address[2];
            this.company.lng = this.formatted_address[3];

            this.company.patch().subscribe(data => {

                console.log(data.plain());

                let alert = this.alertCtrl.create({title: 'Successfully updated', buttons: ['OK']});
                alert.present();

            });

        }

    }

    deleteLogo () {

        for (let file of this.company.medias){

            if (file.pivot.tag === 'logo'){

                this.restangular.one('files', file.id).remove().subscribe(data => {

                    console.log(data);

                    let alert = this.alertCtrl.create({title: 'Successfully deleted', buttons: ['OK']});
                    alert.present();

                    this.restangular.one('companies', this.company_id).get().subscribe(data => {this.company = data;});

                });

            }

        }

    }


    deleteVideo () {

        for (let file of this.company.medias){

            if (file.pivot.tag === 'main_video'){

                this.restangular.one('files', file.id).remove().subscribe(data => {

                    console.log(data);

                    let alert = this.alertCtrl.create({title: 'Successfully deleted', buttons: ['OK']});
                    alert.present();

                    this.restangular.one('companies', this.company_id).get().subscribe(data => {this.company = data;});

                });

            }

        }

    }

    setLogo(logo: any) {

        console.log('Logo received', logo);
        this.newLogo = logo.url;

        this.localProgress.logo = true;
        this.utils.setUploadProcess(true);

        const fileTransfer: FileTransferObject = this.transfer.create();

        let options: FileUploadOptions = {
            fileKey: 'logo',
            mimeType: logo.type,
            fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
            headers: {Authorization: `Bearer ${this.auth.getToken()}`},
            params: {media_key: 'logo', entity_type: 'company', 'entity_id': this.company_id}
        };

        fileTransfer.onProgress((event: ProgressEvent) => {

            if (event.lengthComputable) {

                let progress = Math.round((event.loaded / event.total) * 100);

                console.log('Progress', progress);

                this.utils.setProgress(progress);

            }

        });

        fileTransfer.upload(this.newLogo, 'http://app.y-travel.net/admin_api/v1/files', options)
            .then((data) => {

                console.log(data);

                let newData = JSON.parse(data.response);
                this.company = this.restangular.restangularizeElement('', newData.data, 'companies');

                let alert = this.alertCtrl.create({title: 'Logo is successfully uploaded', buttons: ['OK']});
                alert.present();

                this.localProgress.logo = false;
                this.utils.setUploadProcess(false);
                this.utils.setProgress(0);

            }, (err) => {

                console.log(err);

                let alert = this.alertCtrl.create({title: 'Error', buttons: ['OK']});
                alert.present();

                this.localProgress.logo = false;
                this.utils.setUploadProcess(false);
                this.utils.setProgress(0);

            })
    }

    updateLogo(logo: any) {

        console.log('Logo received', logo);
        this.newLogo = logo.url;

        this.localProgress.logo = true;
        this.utils.setUploadProcess(true);

        const fileTransfer: FileTransferObject = this.transfer.create();

        let logo_id = 0;

        for (let file of this.company.medias){

            if (file.pivot.tag === 'logo')
                logo_id = file.id;

        }

        let options: FileUploadOptions = {
            fileKey: 'logo',
            mimeType: logo.type,
            fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
            headers: {Authorization: `Bearer ${this.auth.getToken()}`},
            params: {media_key: 'logo', entity_type: 'company', entity_id: this.company_id}
        };

        fileTransfer.onProgress((event: ProgressEvent) => {

            if (event.lengthComputable) {

                let progress = Math.round((event.loaded / event.total) * 100);

                console.log('Progress', progress);

                this.utils.setProgress(progress);

            }

        });

        fileTransfer.upload(this.newLogo, 'http://app.y-travel.net/admin_api/v1/files/' + logo_id + '?_method=PATCH', options)
            .then((data) => {

                console.log(data);
                let newData = JSON.parse(data.response);
                this.company = this.restangular.restangularizeElement('', newData.data, 'companies');

                let alert = this.alertCtrl.create({title: 'Logo is successfully changed', buttons: ['OK']});
                alert.present();

                this.localProgress.logo = false;
                this.utils.setUploadProcess(false);
                this.utils.setProgress(0);

            }, (err) => {

                console.log(err);
                let alert = this.alertCtrl.create({title: 'Error', buttons: ['OK']});
                alert.present();

                this.localProgress.logo = false;
                this.utils.setUploadProcess(false);
                this.utils.setProgress(0);

            })
    }

    setVideo(video: any) {

        console.log('Video received', video);
        this.newVideo = video;
        this.src = video.fullPath;

        this.localProgress.video = true;
        this.utils.setUploadProcess(true);

        const fileTransfer: FileTransferObject = this.transfer.create();

        let options: FileUploadOptions = {
            fileKey: 'main_video',
            fileName: this.newVideo.name,
            headers: {Authorization: `Bearer ${this.auth.getToken()}`},
            params: {media_key: 'main_video', entity_type: 'company', 'entity_id': this.company_id}
        };

        fileTransfer.onProgress((event: ProgressEvent) => {

            if (event.lengthComputable) {

                let progress = Math.round((event.loaded / event.total) * 100);

                console.log('Progress', progress);

                this.utils.setProgress(progress);

            }

        });

        fileTransfer.upload(this.newVideo.fullPath, 'http://app.y-travel.net/admin_api/v1/files', options)
            .then((data) => {

                // loading.dismiss();
                console.log(data);
                let newData = JSON.parse(data.response);
                this.company = this.restangular.restangularizeElement('', newData.data, 'companies');

                let alert = this.alertCtrl.create({title: 'Video is successfully uploaded', buttons: ['OK']});
                alert.present();

                this.localProgress.video = false;
                this.utils.setUploadProcess(false);
                this.utils.setProgress(0);

            }, (err) => {

                // loading.dismiss();
                console.log(err);
                let alert = this.alertCtrl.create({title: 'Error', buttons: ['OK']});
                alert.present();

                this.localProgress.video = false;
                this.utils.setUploadProcess(false);
                this.utils.setProgress(0);

            })
    }

    updateVideo(video: any) {

        console.log('Video received', video);
        this.newVideo = video;
        this.src = video.fullPath;

        this.localProgress.video = true;
        this.utils.setUploadProcess(true);

        const fileTransfer: FileTransferObject = this.transfer.create();

        let video_id = 0;

        for (let file of this.company.medias){

            if (file.pivot.tag === 'main_video'){

                video_id = file.id;

            }

        }

        let options: FileUploadOptions = {
            fileKey: 'main_video',
            fileName: this.newVideo.name,
            headers: {Authorization: `Bearer ${this.auth.getToken()}`},
            params: {media_key: 'main_video', entity_type: 'company', entity_id: this.company_id}
        };

        fileTransfer.onProgress((event: ProgressEvent) => {

            if (event.lengthComputable) {

                let progress = Math.round((event.loaded / event.total) * 100);

                console.log('Progress', progress);

                this.utils.setProgress(progress);

            }

        });

        fileTransfer.upload(this.newVideo.fullPath, 'http://app.y-travel.net/admin_api/v1/files/' + video_id + '?_method=PATCH', options)
            .then((data) => {

                // loading.dismiss();
                console.log(data);
                let newData = JSON.parse(data.response);
                this.company = this.restangular.restangularizeElement('', newData.data, 'companies');

                let alert = this.alertCtrl.create({title: 'Video is successfully uploaded', buttons: ['OK']});
                alert.present();

                this.localProgress.video = false;
                this.utils.setUploadProcess(false);
                this.utils.setProgress(0);

            }, (err) => {

                // loading.dismiss();
                console.log(err);
                let alert = this.alertCtrl.create({title: 'Error', buttons: ['OK']});
                alert.present();

                this.localProgress.video = false;
                this.utils.setUploadProcess(false);
                this.utils.setProgress(0);

            })
    }


    async ionViewWillEnter() {
        try {
            this.company = await this.restangular.one('companies', this.company_id).customGET('admin').toPromise();
            this.formatted_address = [this.company.address, this.company.country, this.company.lat, this.company.lng];
            setTimeout(() => {this.getAddress();}, 100)
        } catch (err) {
            console.log(err);
        }

    }

    getAddress() {

        this.loader.load().then(() => {

            let address = document.getElementById('company_profile_address').getElementsByTagName('input')[0];
            let autocomplete = new google.maps.places.Autocomplete(address);

            autocomplete.addListener("place_changed", () => {

                let place = autocomplete.getPlace();
                console.log(place);
                let countryComponent = '';

                for (let item of place.address_components){

                    if (item.types[0] === 'country')
                        countryComponent = item.short_name;

                }

                this.formatted_address = [place.formatted_address, countryComponent, place.geometry.location.lat(), place.geometry.location.lng()];
                console.log(this.formatted_address);

            });


        });

    }

}
