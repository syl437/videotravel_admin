import {Component} from '@angular/core';
import {NavController} from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import {Restangular} from "ngx-restangular";

@Component({
    selector: 'page-scan',
    templateUrl: 'scan.html',
})
export class ScanPage {

    modeOn: boolean = false;
    exists: boolean = false;

    constructor(public navCtrl: NavController,
                private qrScanner: QRScanner,
                public restangular: Restangular) {
    }

    scan () {

        this.modeOn = true;

        this.qrScanner.prepare()

            .then((status: QRScannerStatus) => {

            if (status.authorized) {

                let scanSub = this.qrScanner.scan().subscribe((text: string) => {

                    console.log('Scanned', text);

                    this.modeOn = false;

                    this.qrScanner.hide();
                    scanSub.unsubscribe();

                    this.restangular.all('qr').customGET('', {code: text}).subscribe(data => {

                        this.exists = true;

                    });

                });

                this.qrScanner.show();

                } else if (status.denied) {

                } else {

                }
            })
            .catch((e: any) => console.log('Error is', e));

    }

    ionViewDidLeave () {

        this.modeOn = false;
        this.exists = false;

    }

}
