import {Component} from '@angular/core';
import {InitServiceProvider} from "../../providers/init-service/init-service";
import {Restangular} from "ngx-restangular";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {AlertController} from "ionic-angular";

@Component({
    selector: 'page-points',
    templateUrl: 'points.html',
})
export class PointsPage {

    phone: string = '';
    countries = [];
    points: Array<any> = this.init.points;
    id: number = this.auth.id;
    country: string = "IL";
    language: string = 'he';

    constructor(public init: InitServiceProvider,
                public auth: AuthServiceProvider,
                public restangular: Restangular,
                public alertCtrl: AlertController) {}

    sendPoints (quantity) {

        console.log(this.phone, this.country);

        if (this.phone === '' || this.country === '' || typeof this.country === 'undefined'){
            let alert = this.alertCtrl.create({title: 'Please enter all data!', buttons: ['OK']});
            alert.present();
            return;
        }

        this.restangular.all('codes').post({manager_id: this.id, quantity: quantity, phone: this.phone, country: this.country, language: this.language})
            .subscribe(data => {

                let alert = this.alertCtrl.create({title: 'Successfully sent to ' + data.phone, buttons: ['OK']});
                alert.present();

            });

    }

    ionViewWillEnter () {
        this.restangular.all('init/countries').getList().subscribe(data => this.countries = data);
    }

    ionViewDidLeave () {
        this.phone = '';
        this.country = "IL";
    }

}
