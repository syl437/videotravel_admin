import {Component} from '@angular/core';
import {AlertController, LoadingController, NavParams, ViewController} from 'ionic-angular';
import {Restangular} from "ngx-restangular";
import {UtilsProvider} from "../../providers/utils/utils";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {InitServiceProvider} from "../../providers/init-service/init-service";
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";

@Component({
    selector: 'page-edit-coupon',
    templateUrl: 'edit-coupon.html',
})
export class EditCouponPage {

    coupon: any = {title_en: '', title_he: '', price: '', quantity: '', description_en: '',  description_he: ''};
    image: any = {url: '', type: ''};

    constructor(public restangular: Restangular,
                public viewCtrl: ViewController,
                public utils: UtilsProvider,
                public auth: AuthServiceProvider,
                public init: InitServiceProvider,
                public alertCtrl: AlertController,
                public transfer: FileTransfer,
                public navParams: NavParams,
                public loadingCtrl: LoadingController) {
    }

    closeModal() {this.viewCtrl.dismiss();}

    addImage (img) {
        console.log('Image received', img);
        this.image = img;
    }

    deleteImage () {

        this.restangular.one('files', this.coupon.media[0].id).remove().subscribe(() => {

            let alert = this.alertCtrl.create({title: 'Successfully deleted', buttons: ['OK']});
            alert.present();

            this.restangular.one('coupons', this.coupon.id).get().subscribe(data => {this.coupon = data;});

        });

    }

    deleteLocalImage () {
        this.image = {url: '', type: ''} ;
    }

    editCoupon() {

        console.log(this.coupon);

        if (this.coupon.title_en === '' || this.coupon.price === '' || this.coupon.quantity === ''){

            let alert = this.alertCtrl.create({title: 'נא למלא את כל השדות', buttons: ['OK']});
            alert.present();

            return;

        }

        let loading = this.loadingCtrl.create({content: 'Please wait...'});
        loading.present();

        console.log(this.coupon);

        this.coupon.patch().subscribe(() => {

            // data => this.viewCtrl.dismiss(data)

            if (this.image.url !== ''){

                const fileTransfer: FileTransferObject = this.transfer.create();

                let options: FileUploadOptions = {
                    fileKey: 'coupon',
                    mimeType: this.image.type,
                    fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
                    headers: {Authorization: `Bearer ${this.auth.getToken()}`},
                    params: {media_key: 'coupon', entity_type: 'coupon', 'entity_id': this.coupon.id}
                };

                fileTransfer.upload(this.image.url, 'http://app.y-travel.net/admin_api/v1/files/' + this.coupon.id + '?_method=PATCH', options)
                    .then((data) => {

                        loading.dismiss();
                        this.getCoupons();

                    }, (err) => {

                        console.log(err);

                        loading.dismiss();

                        let alert = this.alertCtrl.create({title: 'Error', buttons: ['OK']});
                        alert.present();

                    })


            } else {

                loading.dismiss();
                this.getCoupons();

            }

        });

    }

    getCoupons () {

        this.restangular.one('companies', this.auth.company_id).all('coupons').getList().subscribe(data => {

            console.log(data);
            this.viewCtrl.dismiss(data);

        })

    }


    ionViewWillEnter() {
        this.restangular.one('coupons', this.navParams.get('coupon_id')).customGET('allTranslations').subscribe(data => {
            this.coupon = data;
        })
    }
}
