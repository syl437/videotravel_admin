import {Component} from '@angular/core';
import {AlertController, LoadingController, ViewController} from 'ionic-angular';
import {Restangular} from "ngx-restangular";
import {UtilsProvider} from "../../providers/utils/utils";
import {AuthServiceProvider} from "../../providers/auth-service/auth-service";
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";

@Component({
    selector: 'page-add-coupon',
    templateUrl: 'add-coupon.html',
})
export class AddCouponPage {

    coupon: any = this.restangular.restangularizeElement('', {title_en: '', title_he: '', price: '', quantity: '', description_en: '', description_he: ''}, 'coupons');
    image: any = {url: '', type: ''};

    constructor(public restangular: Restangular,
                public viewCtrl: ViewController,
                public utils: UtilsProvider,
                public auth: AuthServiceProvider,
                public transfer: FileTransfer,
                public alertCtrl: AlertController,
                public loadingCtrl: LoadingController) {}

    closeModal () {this.viewCtrl.dismiss();}

    addImage (img: any) {
        console.log('Image received', img);
        this.image = img;
    }

    deleteImage () {
        this.image = {url: '', type: ''} ;
    }

    addCoupon () {

        console.log(this.coupon.plain());

        if (!this.utils.isAnyFieldEmpty(this.coupon.plain())){

            let loading = this.loadingCtrl.create({content: 'Please wait...'});
            loading.present();

            this.coupon.company_id = this.auth.company_id;

            this.coupon.save().subscribe(data => {

                if (this.image.url !== ''){

                    const fileTransfer: FileTransferObject = this.transfer.create();

                    let options: FileUploadOptions = {
                        fileKey: 'coupon',
                        mimeType: this.image.type,
                        fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
                        headers: {Authorization: `Bearer ${this.auth.getToken()}`},
                        params: {media_key: 'coupon', entity_type: 'coupon', 'entity_id': data.id}
                    };

                    fileTransfer.upload(this.image.url, 'http://app.y-travel.net/admin_api/v1/files', options)
                        .then((data) => {

                            loading.dismiss();
                            this.getCoupons();

                        }, (err) => {

                            console.log(err);

                            loading.dismiss();

                            let alert = this.alertCtrl.create({title: 'Error', buttons: ['OK']});
                            alert.present();

                        })

                } else {
                    loading.dismiss();
                    this.getCoupons();
                }


            });

        }

    }

    getCoupons () {
        this.restangular.one('companies', this.auth.company_id).all('coupons').getList().subscribe(data => {this.viewCtrl.dismiss(data);})
    }

}
