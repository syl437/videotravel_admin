import {Component, Input, NgZone} from '@angular/core';
import {UtilsProvider} from '../../providers/utils/utils';

@Component({
    selector: 'common-header',
    templateUrl: 'common-header.html'
})
export class CommonHeaderComponent {

    @Input()
    title: string;

    full: number = 0;
    empty: number = 100;
    uploadProcess: boolean = false;

    constructor(private zone: NgZone, public utils: UtilsProvider) {

        this.utils.progress$.subscribe(full => {

            this.zone.run(() => {
                this.full = full;
                this.empty = 100 - full;
            });
        });

        this.utils.uploadProcess$.subscribe(value => this.uploadProcess = value);
    }

}
