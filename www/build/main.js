webpackJsonp([0],{

/***/ 206:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CompanyProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_init_service_init_service__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_restangular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_auth_service_auth_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__agm_core__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_utils_utils__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var CompanyProfilePage = (function () {
    function CompanyProfilePage(storage, restangular, init, alertCtrl, transfer, auth, loader, utils, zone) {
        var _this = this;
        this.storage = storage;
        this.restangular = restangular;
        this.init = init;
        this.alertCtrl = alertCtrl;
        this.transfer = transfer;
        this.auth = auth;
        this.loader = loader;
        this.utils = utils;
        this.zone = zone;
        this.company_id = this.auth.company_id;
        this.company_categories = this.init.company_categories;
        this.regions = this.init.regions;
        this.localProgress = { logo: false, video: false };
        this.process = this.utils.uploadProcess;
        this.utils.progress$.subscribe(function (val) {
            _this.zone.run(function () { _this.progress = val; });
        });
    }
    CompanyProfilePage.prototype.update = function () {
        var _this = this;
        console.log(this.company.plain());
        delete this.company.website;
        delete this.company.approved;
        delete this.company.from_user;
        delete this.company.pin;
        delete this.company.rate;
        delete this.company.stars;
        delete this.company.thumbnail;
        delete this.company.main_video;
        delete this.company.logo;
        delete this.company.translations;
        if (!this.utils.isAnyFieldEmpty(this.company.plain())) {
            this.company.address = this.formatted_address[0];
            this.company.country = this.formatted_address[1];
            this.company.lat = this.formatted_address[2];
            this.company.lng = this.formatted_address[3];
            this.company.patch().subscribe(function (data) {
                console.log(data.plain());
                var alert = _this.alertCtrl.create({ title: 'Successfully updated', buttons: ['OK'] });
                alert.present();
            });
        }
    };
    CompanyProfilePage.prototype.deleteLogo = function () {
        var _this = this;
        for (var _i = 0, _a = this.company.medias; _i < _a.length; _i++) {
            var file = _a[_i];
            if (file.pivot.tag === 'logo') {
                this.restangular.one('files', file.id).remove().subscribe(function (data) {
                    console.log(data);
                    var alert = _this.alertCtrl.create({ title: 'Successfully deleted', buttons: ['OK'] });
                    alert.present();
                    _this.restangular.one('companies', _this.company_id).get().subscribe(function (data) { _this.company = data; });
                });
            }
        }
    };
    CompanyProfilePage.prototype.deleteVideo = function () {
        var _this = this;
        for (var _i = 0, _a = this.company.medias; _i < _a.length; _i++) {
            var file = _a[_i];
            if (file.pivot.tag === 'main_video') {
                this.restangular.one('files', file.id).remove().subscribe(function (data) {
                    console.log(data);
                    var alert = _this.alertCtrl.create({ title: 'Successfully deleted', buttons: ['OK'] });
                    alert.present();
                    _this.restangular.one('companies', _this.company_id).get().subscribe(function (data) { _this.company = data; });
                });
            }
        }
    };
    CompanyProfilePage.prototype.setLogo = function (logo) {
        var _this = this;
        console.log('Logo received', logo);
        this.newLogo = logo.url;
        this.localProgress.logo = true;
        this.utils.setUploadProcess(true);
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: 'logo',
            mimeType: logo.type,
            fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
            headers: { Authorization: "Bearer " + this.auth.getToken() },
            params: { media_key: 'logo', entity_type: 'company', 'entity_id': this.company_id }
        };
        fileTransfer.onProgress(function (event) {
            if (event.lengthComputable) {
                var progress = Math.round((event.loaded / event.total) * 100);
                console.log('Progress', progress);
                _this.utils.setProgress(progress);
            }
        });
        fileTransfer.upload(this.newLogo, 'http://app.y-travel.net/admin_api/v1/files', options)
            .then(function (data) {
            console.log(data);
            var newData = JSON.parse(data.response);
            _this.company = _this.restangular.restangularizeElement('', newData.data, 'companies');
            var alert = _this.alertCtrl.create({ title: 'Logo is successfully uploaded', buttons: ['OK'] });
            alert.present();
            _this.localProgress.logo = false;
            _this.utils.setUploadProcess(false);
            _this.utils.setProgress(0);
        }, function (err) {
            console.log(err);
            var alert = _this.alertCtrl.create({ title: 'Error', buttons: ['OK'] });
            alert.present();
            _this.localProgress.logo = false;
            _this.utils.setUploadProcess(false);
            _this.utils.setProgress(0);
        });
    };
    CompanyProfilePage.prototype.updateLogo = function (logo) {
        var _this = this;
        console.log('Logo received', logo);
        this.newLogo = logo.url;
        this.localProgress.logo = true;
        this.utils.setUploadProcess(true);
        var fileTransfer = this.transfer.create();
        var logo_id = 0;
        for (var _i = 0, _a = this.company.medias; _i < _a.length; _i++) {
            var file = _a[_i];
            if (file.pivot.tag === 'logo')
                logo_id = file.id;
        }
        var options = {
            fileKey: 'logo',
            mimeType: logo.type,
            fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
            headers: { Authorization: "Bearer " + this.auth.getToken() },
            params: { media_key: 'logo', entity_type: 'company', entity_id: this.company_id }
        };
        fileTransfer.onProgress(function (event) {
            if (event.lengthComputable) {
                var progress = Math.round((event.loaded / event.total) * 100);
                console.log('Progress', progress);
                _this.utils.setProgress(progress);
            }
        });
        fileTransfer.upload(this.newLogo, 'http://app.y-travel.net/admin_api/v1/files/' + logo_id + '?_method=PATCH', options)
            .then(function (data) {
            console.log(data);
            var newData = JSON.parse(data.response);
            _this.company = _this.restangular.restangularizeElement('', newData.data, 'companies');
            var alert = _this.alertCtrl.create({ title: 'Logo is successfully changed', buttons: ['OK'] });
            alert.present();
            _this.localProgress.logo = false;
            _this.utils.setUploadProcess(false);
            _this.utils.setProgress(0);
        }, function (err) {
            console.log(err);
            var alert = _this.alertCtrl.create({ title: 'Error', buttons: ['OK'] });
            alert.present();
            _this.localProgress.logo = false;
            _this.utils.setUploadProcess(false);
            _this.utils.setProgress(0);
        });
    };
    CompanyProfilePage.prototype.setVideo = function (video) {
        var _this = this;
        console.log('Video received', video);
        this.newVideo = video;
        this.src = video.fullPath;
        this.localProgress.video = true;
        this.utils.setUploadProcess(true);
        var fileTransfer = this.transfer.create();
        var options = {
            fileKey: 'main_video',
            fileName: this.newVideo.name,
            headers: { Authorization: "Bearer " + this.auth.getToken() },
            params: { media_key: 'main_video', entity_type: 'company', 'entity_id': this.company_id }
        };
        fileTransfer.onProgress(function (event) {
            if (event.lengthComputable) {
                var progress = Math.round((event.loaded / event.total) * 100);
                console.log('Progress', progress);
                _this.utils.setProgress(progress);
            }
        });
        fileTransfer.upload(this.newVideo.fullPath, 'http://app.y-travel.net/admin_api/v1/files', options)
            .then(function (data) {
            // loading.dismiss();
            console.log(data);
            var newData = JSON.parse(data.response);
            _this.company = _this.restangular.restangularizeElement('', newData.data, 'companies');
            var alert = _this.alertCtrl.create({ title: 'Video is successfully uploaded', buttons: ['OK'] });
            alert.present();
            _this.localProgress.video = false;
            _this.utils.setUploadProcess(false);
            _this.utils.setProgress(0);
        }, function (err) {
            // loading.dismiss();
            console.log(err);
            var alert = _this.alertCtrl.create({ title: 'Error', buttons: ['OK'] });
            alert.present();
            _this.localProgress.video = false;
            _this.utils.setUploadProcess(false);
            _this.utils.setProgress(0);
        });
    };
    CompanyProfilePage.prototype.updateVideo = function (video) {
        var _this = this;
        console.log('Video received', video);
        this.newVideo = video;
        this.src = video.fullPath;
        this.localProgress.video = true;
        this.utils.setUploadProcess(true);
        var fileTransfer = this.transfer.create();
        var video_id = 0;
        for (var _i = 0, _a = this.company.medias; _i < _a.length; _i++) {
            var file = _a[_i];
            if (file.pivot.tag === 'main_video') {
                video_id = file.id;
            }
        }
        var options = {
            fileKey: 'main_video',
            fileName: this.newVideo.name,
            headers: { Authorization: "Bearer " + this.auth.getToken() },
            params: { media_key: 'main_video', entity_type: 'company', entity_id: this.company_id }
        };
        fileTransfer.onProgress(function (event) {
            if (event.lengthComputable) {
                var progress = Math.round((event.loaded / event.total) * 100);
                console.log('Progress', progress);
                _this.utils.setProgress(progress);
            }
        });
        fileTransfer.upload(this.newVideo.fullPath, 'http://app.y-travel.net/admin_api/v1/files/' + video_id + '?_method=PATCH', options)
            .then(function (data) {
            // loading.dismiss();
            console.log(data);
            var newData = JSON.parse(data.response);
            _this.company = _this.restangular.restangularizeElement('', newData.data, 'companies');
            var alert = _this.alertCtrl.create({ title: 'Video is successfully uploaded', buttons: ['OK'] });
            alert.present();
            _this.localProgress.video = false;
            _this.utils.setUploadProcess(false);
            _this.utils.setProgress(0);
        }, function (err) {
            // loading.dismiss();
            console.log(err);
            var alert = _this.alertCtrl.create({ title: 'Error', buttons: ['OK'] });
            alert.present();
            _this.localProgress.video = false;
            _this.utils.setUploadProcess(false);
            _this.utils.setProgress(0);
        });
    };
    CompanyProfilePage.prototype.ionViewWillEnter = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var _a, err_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.restangular.one('companies', this.company_id).customGET('admin').toPromise()];
                    case 1:
                        _a.company = _b.sent();
                        this.formatted_address = [this.company.address, this.company.country, this.company.lat, this.company.lng];
                        setTimeout(function () { _this.getAddress(); }, 100);
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _b.sent();
                        console.log(err_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    CompanyProfilePage.prototype.getAddress = function () {
        var _this = this;
        this.loader.load().then(function () {
            var address = document.getElementById('company_profile_address').getElementsByTagName('input')[0];
            var autocomplete = new google.maps.places.Autocomplete(address);
            autocomplete.addListener("place_changed", function () {
                var place = autocomplete.getPlace();
                console.log(place);
                var countryComponent = '';
                for (var _i = 0, _a = place.address_components; _i < _a.length; _i++) {
                    var item = _a[_i];
                    if (item.types[0] === 'country')
                        countryComponent = item.short_name;
                }
                _this.formatted_address = [place.formatted_address, countryComponent, place.geometry.location.lat(), place.geometry.location.lng()];
                console.log(_this.formatted_address);
            });
        });
    };
    return CompanyProfilePage;
}());
CompanyProfilePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-company-profile',template:/*ion-inline-start:"G:\github\videotravel_admin\src\pages\company-profile\company-profile.html"*/'<ion-header>\n\n\n\n    <common-header title="עמוד העסק"></common-header>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n    <div class="div90" text-center *ngIf="company">\n\n\n\n        <ion-grid>\n\n\n\n            <ion-row *ngIf="company && company.logo !== \'\'" align-items-center>\n\n\n\n                <ion-col col-4>\n\n\n\n                    <img [src]="company && company.logo" width="80" *ngIf="localProgress.logo === false">\n\n\n\n                    <div *ngIf="localProgress.logo === true">\n\n                        <ion-spinner name="ios"></ion-spinner>\n\n                        <div>Uploading in process: {{progress}}% </div>\n\n                    </div>\n\n\n\n                </ion-col>\n\n                <ion-col col-4>\n\n                    <button ion-button small [disabled]="localProgress.logo || localProgress.video" color="primary" (click)="deleteLogo()">מחק</button>\n\n                </ion-col>\n\n                <ion-col col-4>\n\n                    <button [disabled]="localProgress.logo || localProgress.video" ion-button small color="primary" add-picture (targetLogo)="updateLogo($event)">עדכן</button>\n\n                </ion-col>\n\n\n\n            </ion-row>\n\n\n\n            <ion-row *ngIf="company.logo === \'\'">\n\n\n\n                <button [disabled]="localProgress.logo || localProgress.video" ion-button block color="primary" add-picture (targetLogo)="setLogo($event)">הוסף תמונה</button>\n\n\n\n            </ion-row>\n\n\n\n            <ion-row *ngIf="company.main_video !== \'\'">\n\n\n\n                <video [src]="company.main_video" controls *ngIf="localProgress.video === false"></video>\n\n\n\n                <div *ngIf="localProgress.video === true" class="videoUpload">\n\n                    <ion-spinner name="ios"></ion-spinner>\n\n                    <div>Uploading in process: {{progress}}%</div>\n\n                </div>\n\n\n\n            </ion-row>\n\n\n\n            <ion-row *ngIf="company.main_video !== \'\'">\n\n\n\n                <ion-col col-6>\n\n                    <button [disabled]="localProgress.logo || localProgress.video" ion-button block color="primary" (click)="deleteVideo()">מחק</button>\n\n                </ion-col>\n\n                <ion-col col-6>\n\n                    <button ion-button [disabled]="localProgress.logo || localProgress.video" block color="primary" add-video (targetVideo)="updateVideo($event)">עדכן</button>\n\n                </ion-col>\n\n\n\n            </ion-row>\n\n\n\n            <ion-row *ngIf="company.main_video === \'\'">\n\n\n\n                <button [disabled]="localProgress.logo || localProgress.video" ion-button block color="primary" add-video (targetVideo)="setVideo($event)">הוסף וידאו</button>\n\n\n\n            </ion-row>\n\n\n\n            <ion-list no-lines>\n\n\n\n                <ion-item margin-top class="deleteLeftPadding">\n\n                    <ion-input text-center placeholder="שם העסק" type="text" [(ngModel)]="company.title_he"></ion-input>\n\n                </ion-item>\n\n\n\n                <ion-item margin-top class="deleteLeftPadding">\n\n                    <ion-input text-center placeholder="Company title" type="text" [(ngModel)]="company.title_en"></ion-input>\n\n                </ion-item>\n\n\n\n                <ion-item margin-top class="deleteLeftPadding">\n\n                    <ion-input text-center placeholder="כתובת" type="text" [(ngModel)]="company.address" id="company_profile_address"></ion-input>\n\n                </ion-item>\n\n\n\n                <ion-item margin-top class="deleteLeftPadding">\n\n                    <ion-input text-center placeholder="טלפון" type="tel" [(ngModel)]="company.phone"></ion-input>\n\n                </ion-item>\n\n\n\n                <ion-item margin-top class="deleteLeftPadding customSelect">\n\n                    <ion-label color="primary">קטגוריה</ion-label>\n\n                    <ion-select [(ngModel)]="company.company_category_id">\n\n                        <ion-option *ngFor="let category of company_categories" [value]="category.id">{{category.title}}</ion-option>\n\n                    </ion-select>\n\n                </ion-item>\n\n\n\n                <ion-item margin-top class="deleteLeftPadding customSelect">\n\n                    <ion-label color="primary">תת קטגוריה</ion-label>\n\n                    <ion-select [(ngModel)]="company.company_subcategory_id">\n\n                        <ion-option *ngFor="let subcategory of company_categories[company.company_category_id - 1].company_subcategories" [value]="subcategory.id">{{subcategory.title}}</ion-option>\n\n                    </ion-select>\n\n                </ion-item>\n\n\n\n                <ion-item margin-top class="deleteLeftPadding customSelect">\n\n                    <ion-label color="primary">איזור</ion-label>\n\n                    <ion-select [(ngModel)]="company.region_id">\n\n                        <ion-option *ngFor="let region of regions" [value]="region.id">{{region.title}}</ion-option>\n\n                    </ion-select>\n\n                </ion-item>\n\n\n\n                <ion-item margin-top class="deleteLeftPadding">\n\n                    <ion-textarea text-right rows=\'4\' placeholder="תיאור" [(ngModel)]="company.description_he"></ion-textarea>\n\n                </ion-item>\n\n\n\n                <ion-item margin-top class="deleteLeftPadding">\n\n                    <ion-textarea text-left rows=\'4\' placeholder="Description" [(ngModel)]="company.description_en"></ion-textarea>\n\n                </ion-item>\n\n\n\n                <button [disabled]="localProgress.logo || localProgress.video" margin-top class="buttonNoBorder" ion-button outline no-border color="primary" (click)="update()">\n\n                    <img src="assets/images/arrows_left.png" height="25">&nbsp; <span>עדכן</span>\n\n                </button>\n\n\n\n            </ion-list>\n\n\n\n        </ion-grid>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\videotravel_admin\src\pages\company-profile\company-profile.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_4_ngx_restangular__["Restangular"],
        __WEBPACK_IMPORTED_MODULE_3__providers_init_service_init_service__["a" /* InitServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__["a" /* FileTransfer */],
        __WEBPACK_IMPORTED_MODULE_6__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_7__agm_core__["b" /* MapsAPILoader */],
        __WEBPACK_IMPORTED_MODULE_8__providers_utils_utils__["a" /* UtilsProvider */],
        __WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"]])
], CompanyProfilePage);

//# sourceMappingURL=company-profile.js.map

/***/ }),

/***/ 229:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 229;

/***/ }),

/***/ 272:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	return new Promise(function(resolve, reject) { reject(new Error("Cannot find module '" + req + "'.")); });
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 272;

/***/ }),

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthServiceProvider = (function () {
    function AuthServiceProvider(storage) {
        this.storage = storage;
        this._token = new __WEBPACK_IMPORTED_MODULE_2_rxjs__["BehaviorSubject"]('');
        this.token$ = this._token.asObservable();
    }
    // Reads token from storage (should return promise)
    AuthServiceProvider.prototype.initialize = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.storage.get('token').then(function (token) {
                _this.storage.get('company_id').then(function (val) {
                    _this.storage.get('id').then(function (id) {
                        _this.id = id || null;
                        _this.company_id = val || null;
                        _this._token.next(token || '');
                        _this.token = token || '';
                        resolve();
                    });
                });
            });
        });
    };
    // Getter
    AuthServiceProvider.prototype.getToken = function () {
        return this.token;
    };
    // Setter
    AuthServiceProvider.prototype.setToken = function (token) {
        this.token = token;
        this.storage.set('token', this.token);
        this._token.next(token);
    };
    return AuthServiceProvider;
}());
AuthServiceProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
], AuthServiceProvider);

//# sourceMappingURL=auth-service.js.map

/***/ }),

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__company_profile_company_profile__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_auth_service_auth_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_init_service_init_service__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};







var LoginPage = (function () {
    // manager = {email: 'rona.tbf@gmail.com', password: 'aaayyyaaa'};
    function LoginPage(navCtrl, restangular, alertCtrl, storage, auth, events, init) {
        this.navCtrl = navCtrl;
        this.restangular = restangular;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.auth = auth;
        this.events = events;
        this.init = init;
        this.manager = { email: '', password: '' };
    }
    LoginPage.prototype.authenticate = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var alert_1;
            return __generator(this, function (_a) {
                if (this.manager.email === '' || this.manager.password === '') {
                    alert_1 = this.alertCtrl.create({ title: 'נא למלא את כל השדות', buttons: ['OK'] });
                    alert_1.present();
                    return [2 /*return*/];
                }
                this.restangular.all('tokens/managers').post(this.manager).subscribe(function (data) { return __awaiter(_this, void 0, void 0, function () {
                    var _this = this;
                    return __generator(this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                this.auth.setToken(data.api_token);
                                return [4 /*yield*/, this.storage.set('id', data.id)];
                            case 1:
                                _a.sent();
                                return [4 /*yield*/, this.storage.set('company_id', data.company_id)];
                            case 2:
                                _a.sent();
                                return [4 /*yield*/, this.storage.set('hotel', data.is_hotel)];
                            case 3:
                                _a.sent();
                                this.auth.initialize().then(function () { return _this.init.onInitApp().then(function (success) {
                                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__company_profile_company_profile__["a" /* CompanyProfilePage */]);
                                    _this.events.publish('login:success');
                                }); });
                                return [2 /*return*/];
                        }
                    });
                }); });
                return [2 /*return*/];
            });
        });
    };
    return LoginPage;
}());
LoginPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-company-login',template:/*ion-inline-start:"G:\github\videotravel_admin\src\pages\login\login.html"*/'<ion-header>\n\n\n\n    <ion-navbar color="primary" text-center>\n\n        <ion-title>כניסה</ion-title>\n\n    </ion-navbar>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <div class="div90" text-center>\n\n\n\n        <ion-list no-lines>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input text-center placeholder="מייל" type="email" [(ngModel)]="manager.email"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input text-center placeholder="סיסמא" type="password" [(ngModel)]="manager.password"></ion-input>\n\n            </ion-item>\n\n\n\n        </ion-list>\n\n\n\n        <button class="buttonNoBorder" ion-button outline no-border color="primary" (click)="authenticate()">\n\n            <img src="assets/images/arrows_left.png" height="25">&nbsp; <span>התחבר</span>\n\n        </button>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\videotravel_admin\src\pages\login\login.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_5__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
        __WEBPACK_IMPORTED_MODULE_6__providers_init_service_init_service__["a" /* InitServiceProvider */]])
], LoginPage);

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 402:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_utils_utils__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_restangular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(59);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProfilePage = (function () {
    function ProfilePage(navCtrl, utils, restangular, alertCtrl, auth, storage) {
        this.navCtrl = navCtrl;
        this.utils = utils;
        this.restangular = restangular;
        this.alertCtrl = alertCtrl;
        this.auth = auth;
        this.storage = storage;
        this.data = { name: '', old_password: '', new_password: '' };
        this.id = this.auth.id;
    }
    ProfilePage.prototype.update = function () {
        var _this = this;
        if (this.data.name === '' && (this.data.old_password === '' && this.data.new_password === '')) {
            var alert_1 = this.alertCtrl.create({ title: 'Nothing to change', buttons: ['OK'] });
            alert_1.present();
            return;
        }
        if (this.data.new_password !== '' && this.data.old_password === '') {
            var alert_2 = this.alertCtrl.create({ title: 'Please fill in old password', buttons: ['OK'] });
            alert_2.present();
            return;
        }
        this.restangular.one('managers', this.id).patch(this.data).subscribe(function (data) {
            _this.data.old_password = '';
            _this.data.new_password = '';
            var alert = _this.alertCtrl.create({ title: 'Successfully changed', buttons: ['OK'] });
            alert.present();
        });
    };
    ProfilePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get('id').then(function (val) {
            _this.restangular.one('managers', val).get().subscribe(function (data) {
                _this.data.name = data.name;
            });
        });
    };
    return ProfilePage;
}());
ProfilePage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-profile',template:/*ion-inline-start:"G:\github\videotravel_admin\src\pages\profile\profile.html"*/'<ion-header>\n\n\n\n    <common-header title="עמוד אישי"></common-header>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n    <div class="div90" text-center="">\n\n\n\n        <ion-list no-lines margin-top>\n\n\n\n            <ion-item>\n\n                <ion-input text-center placeholder="שם" type="text" [(ngModel)]="data.name"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top>\n\n                <ion-input text-center placeholder="סיסמה ישנה" type="password" [(ngModel)]="data.old_password"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top>\n\n                <ion-input text-center placeholder="סיסמה חדשה" type="password" [(ngModel)]="data.new_password"></ion-input>\n\n            </ion-item>\n\n\n\n            <button class="buttonNoBorder" margin-top ion-button outline no-border color="primary" (click)="update()">\n\n                <img src="assets/images/arrows_left.png" height="25">&nbsp; <span>עדכן</span>\n\n            </button>\n\n\n\n        </ion-list>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\videotravel_admin\src\pages\profile\profile.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__providers_utils_utils__["a" /* UtilsProvider */],
        __WEBPACK_IMPORTED_MODULE_3_ngx_restangular__["Restangular"],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */]])
], ProfilePage);

//# sourceMappingURL=profile.js.map

/***/ }),

/***/ 403:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EmployeesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_restangular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var EmployeesPage = (function () {
    function EmployeesPage(init, alertCtrl, restangular, auth) {
        this.init = init;
        this.alertCtrl = alertCtrl;
        this.restangular = restangular;
        this.auth = auth;
        this.employees = this.init.managers;
    }
    EmployeesPage.prototype.showPopup = function () {
        var that = this;
        var prompt = that.alertCtrl.create({
            title: 'New employee',
            message: "Enter a name and an email",
            inputs: [
                {
                    name: 'name',
                    placeholder: 'שם'
                },
                {
                    name: 'email',
                    placeholder: 'אימייל'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: function (data) { }
                },
                {
                    text: 'Send',
                    handler: function (data) {
                        if (data.email !== '') {
                            that.restangular.all('managers').post({ name: data.name, email: data.email, company_id: that.auth.company_id })
                                .subscribe(function (data) {
                                that.employees = data;
                                that.init.managers = data;
                            });
                        }
                    }
                }
            ]
        });
        prompt.present();
    };
    EmployeesPage.prototype.deleteEmployee = function (id) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Delete employee?',
            buttons: [
                {
                    text: 'No',
                    handler: function () { }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        _this.restangular.one('managers', id).remove().subscribe(function (data) {
                            _this.employees = data;
                            _this.init.managers = data;
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    return EmployeesPage;
}());
EmployeesPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-employees',template:/*ion-inline-start:"G:\github\videotravel_admin\src\pages\employees\employees.html"*/'<ion-header>\n\n\n\n    <common-header title="עובדים"></common-header>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n    <div class="div90" text-center>\n\n\n\n        <ion-list dir="rtl">\n\n\n\n            <ion-item>\n\n\n\n                <ion-label fixed>שם</ion-label>\n\n                <div item-content text-start>אימייל</div>\n\n                <div item-end text-end>מחק</div>\n\n\n\n            </ion-item>\n\n\n\n            <ion-item *ngFor="let employee of employees">\n\n\n\n                <ion-label fixed>{{employee.name}}</ion-label>\n\n                <div item-content text-start>{{employee.email}}</div>\n\n                <div item-end text-end><ion-icon name="close-circle" (click)="deleteEmployee(employee.id)"></ion-icon></div>\n\n\n\n            </ion-item>\n\n\n\n        </ion-list>\n\n\n\n        <ion-fab right bottom>\n\n            <button ion-fab color="primary" (click)="showPopup()"><ion-icon name="add"></ion-icon></button>\n\n        </ion-fab>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\videotravel_admin\src\pages\employees\employees.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__["a" /* InitServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_3_ngx_restangular__["Restangular"],
        __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__["a" /* AuthServiceProvider */]])
], EmployeesPage);

//# sourceMappingURL=employees.js.map

/***/ }),

/***/ 404:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CouponsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_restangular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__add_coupon_add_coupon__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__edit_coupon_edit_coupon__ = __webpack_require__(406);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var CouponsPage = (function () {
    function CouponsPage(init, alertCtrl, restangular, modalCtrl) {
        this.init = init;
        this.alertCtrl = alertCtrl;
        this.restangular = restangular;
        this.modalCtrl = modalCtrl;
        this.coupons = this.init.coupons;
    }
    CouponsPage.prototype.deleteCoupon = function (id) {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Delete coupon?',
            buttons: [
                {
                    text: 'No',
                    handler: function () { }
                },
                {
                    text: 'Yes',
                    handler: function () {
                        _this.restangular.one('coupons', id).remove().subscribe(function (data) {
                            _this.coupons = data;
                            _this.init.coupons = data;
                        });
                    }
                }
            ]
        });
        confirm.present();
    };
    CouponsPage.prototype.addCoupon = function () {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_4__add_coupon_add_coupon__["a" /* AddCouponPage */]);
        modal.present();
        modal.onDidDismiss(function (data) {
            if (typeof data !== 'undefined') {
                _this.coupons = data;
                _this.init.coupons = data;
            }
        });
    };
    CouponsPage.prototype.editCoupon = function (coupon) {
        var _this = this;
        var modal = this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_5__edit_coupon_edit_coupon__["a" /* EditCouponPage */], { coupon_id: coupon });
        modal.present();
        modal.onDidDismiss(function (data) {
            if (typeof data !== 'undefined') {
                _this.coupons = data;
                _this.init.coupons = data;
            }
        });
    };
    return CouponsPage;
}());
CouponsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-coupons',template:/*ion-inline-start:"G:\github\videotravel_admin\src\pages\coupons\coupons.html"*/'<ion-header>\n\n\n\n    <common-header title="קופונים"></common-header>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n    <ion-grid no-border no-margin no-padding *ngIf="coupons.length > 0">\n\n\n\n            <ion-row nowrap align-items-stretch dir="rtl" *ngFor="let coupon of coupons" style="padding-right: 5px;">\n\n                <ion-col col-2 text-center>\n\n                    <img [src]="coupon.image" *ngIf="coupon.image !== \'\'">\n\n                    <img [src]="init.company.logo"  *ngIf="coupon.image === \'\'">\n\n                    <img src="assets/images/logo.png"  *ngIf="init.company.logo === \'\' && coupon.image === \'\'">\n\n                </ion-col>\n\n                <ion-col col-2 align-self-center text-center>\n\n                    <div class="couponQuantity primaryColor"><span>{{coupon.quantity}}</span></div>\n\n                </ion-col>\n\n                <ion-col col-6 text-right>\n\n                    <div><b>{{coupon.title}}</b></div>\n\n                    <div class="couponPrice"> {{coupon.price}} נקודות</div>\n\n                </ion-col>\n\n                <ion-col col-1 text-center align-self-center>\n\n                    <h1><ion-icon name="create" color="primary" (click)="editCoupon(coupon.id)"></ion-icon></h1>\n\n                </ion-col>\n\n                <ion-col col-1 text-center align-self-center>\n\n                    <h1><ion-icon name="trash" color="primary" (click)="deleteCoupon(coupon.id)"></ion-icon></h1>\n\n                </ion-col>\n\n            </ion-row>\n\n\n\n    </ion-grid>\n\n\n\n    <ion-fab right bottom>\n\n        <button ion-fab color="primary" (click)="addCoupon()"><ion-icon name="add"></ion-icon></button>\n\n    </ion-fab>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\videotravel_admin\src\pages\coupons\coupons.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_init_service_init_service__["a" /* InitServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_3_ngx_restangular__["Restangular"],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
], CouponsPage);

//# sourceMappingURL=coupons.js.map

/***/ }),

/***/ 405:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddCouponPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__ = __webpack_require__(122);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AddCouponPage = (function () {
    function AddCouponPage(restangular, viewCtrl, utils, auth, transfer, alertCtrl, loadingCtrl) {
        this.restangular = restangular;
        this.viewCtrl = viewCtrl;
        this.utils = utils;
        this.auth = auth;
        this.transfer = transfer;
        this.alertCtrl = alertCtrl;
        this.loadingCtrl = loadingCtrl;
        this.coupon = this.restangular.restangularizeElement('', { title_en: '', title_he: '', price: '', quantity: '', description_en: '', description_he: '' }, 'coupons');
        this.image = { url: '', type: '' };
    }
    AddCouponPage.prototype.closeModal = function () { this.viewCtrl.dismiss(); };
    AddCouponPage.prototype.addImage = function (img) {
        console.log('Image received', img);
        this.image = img;
    };
    AddCouponPage.prototype.deleteImage = function () {
        this.image = { url: '', type: '' };
    };
    AddCouponPage.prototype.addCoupon = function () {
        var _this = this;
        console.log(this.coupon.plain());
        if (!this.utils.isAnyFieldEmpty(this.coupon.plain())) {
            var loading_1 = this.loadingCtrl.create({ content: 'Please wait...' });
            loading_1.present();
            this.coupon.company_id = this.auth.company_id;
            this.coupon.save().subscribe(function (data) {
                if (_this.image.url !== '') {
                    var fileTransfer = _this.transfer.create();
                    var options = {
                        fileKey: 'coupon',
                        mimeType: _this.image.type,
                        fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
                        headers: { Authorization: "Bearer " + _this.auth.getToken() },
                        params: { media_key: 'coupon', entity_type: 'coupon', 'entity_id': data.id }
                    };
                    fileTransfer.upload(_this.image.url, 'http://app.y-travel.net/admin_api/v1/files', options)
                        .then(function (data) {
                        loading_1.dismiss();
                        _this.getCoupons();
                    }, function (err) {
                        console.log(err);
                        loading_1.dismiss();
                        var alert = _this.alertCtrl.create({ title: 'Error', buttons: ['OK'] });
                        alert.present();
                    });
                }
                else {
                    loading_1.dismiss();
                    _this.getCoupons();
                }
            });
        }
    };
    AddCouponPage.prototype.getCoupons = function () {
        var _this = this;
        this.restangular.one('companies', this.auth.company_id).all('coupons').getList().subscribe(function (data) { _this.viewCtrl.dismiss(data); });
    };
    return AddCouponPage;
}());
AddCouponPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-add-coupon',template:/*ion-inline-start:"G:\github\videotravel_admin\src\pages\add-coupon\add-coupon.html"*/'<ion-header>\n\n\n\n    <ion-navbar color="primary" text-center>\n\n        <ion-title>הוסף קופון</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button (click)="closeModal()">\n\n                סגור\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <div class="div90" text-center>\n\n\n\n        <ion-row *ngIf="image.url !== \'\'" align-items-center>\n\n\n\n            <ion-col col-4>\n\n\n\n                <img [src]="image.url" width="80">\n\n\n\n            </ion-col>\n\n            <ion-col col-4>\n\n                <button ion-button small color="primary" (click)="deleteImage()">מחק</button>\n\n            </ion-col>\n\n            <ion-col col-4>\n\n                <button ion-button small color="primary" add-picture (targetLogo)="addImage($event)">עדכן</button>\n\n            </ion-col>\n\n\n\n        </ion-row>\n\n\n\n        <ion-row *ngIf="image.url === \'\'">\n\n\n\n            <button ion-button block color="primary" add-picture (targetLogo)="addImage($event)">הוסף תמונה</button>\n\n\n\n        </ion-row>\n\n\n\n        <ion-list no-lines>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input text-center placeholder="כותרת באנגלית" type="text" [(ngModel)]="coupon.title_en"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input text-center placeholder="כותרת בעברית" type="text" [(ngModel)]="coupon.title_he"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input text-center placeholder="מחיר" type="number" [(ngModel)]="coupon.price"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input text-center placeholder="כמות" type="number" [(ngModel)]="coupon.quantity"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-textarea placeholder="תיאור באנגלית" text-right [(ngModel)]="coupon.description_en"></ion-textarea>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-textarea placeholder="תיואר בעברית" text-right [(ngModel)]="coupon.description_he"></ion-textarea>\n\n            </ion-item>\n\n\n\n        </ion-list>\n\n\n\n        <button class="buttonNoBorder" ion-button outline no-border color="primary" (click)="addCoupon()">\n\n            <img src="assets/images/arrows_left.png" height="17">&nbsp; שלח\n\n        </button>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\videotravel_admin\src\pages\add-coupon\add-coupon.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__["a" /* UtilsProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_transfer__["a" /* FileTransfer */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
], AddCouponPage);

//# sourceMappingURL=add-coupon.js.map

/***/ }),

/***/ 406:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditCouponPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_init_service_init_service__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_transfer__ = __webpack_require__(122);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var EditCouponPage = (function () {
    function EditCouponPage(restangular, viewCtrl, utils, auth, init, alertCtrl, transfer, navParams, loadingCtrl) {
        this.restangular = restangular;
        this.viewCtrl = viewCtrl;
        this.utils = utils;
        this.auth = auth;
        this.init = init;
        this.alertCtrl = alertCtrl;
        this.transfer = transfer;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.coupon = { title_en: '', title_he: '', price: '', quantity: '', description_en: '', description_he: '' };
        this.image = { url: '', type: '' };
    }
    EditCouponPage.prototype.closeModal = function () { this.viewCtrl.dismiss(); };
    EditCouponPage.prototype.addImage = function (img) {
        console.log('Image received', img);
        this.image = img;
    };
    EditCouponPage.prototype.deleteImage = function () {
        var _this = this;
        this.restangular.one('files', this.coupon.media[0].id).remove().subscribe(function () {
            var alert = _this.alertCtrl.create({ title: 'Successfully deleted', buttons: ['OK'] });
            alert.present();
            _this.restangular.one('coupons', _this.coupon.id).get().subscribe(function (data) { _this.coupon = data; });
        });
    };
    EditCouponPage.prototype.deleteLocalImage = function () {
        this.image = { url: '', type: '' };
    };
    EditCouponPage.prototype.editCoupon = function () {
        var _this = this;
        console.log(this.coupon);
        if (this.coupon.title_en === '' || this.coupon.price === '' || this.coupon.quantity === '') {
            var alert_1 = this.alertCtrl.create({ title: 'נא למלא את כל השדות', buttons: ['OK'] });
            alert_1.present();
            return;
        }
        var loading = this.loadingCtrl.create({ content: 'Please wait...' });
        loading.present();
        console.log(this.coupon);
        this.coupon.patch().subscribe(function () {
            // data => this.viewCtrl.dismiss(data)
            if (_this.image.url !== '') {
                var fileTransfer = _this.transfer.create();
                var options = {
                    fileKey: 'coupon',
                    mimeType: _this.image.type,
                    fileName: Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.jpg',
                    headers: { Authorization: "Bearer " + _this.auth.getToken() },
                    params: { media_key: 'coupon', entity_type: 'coupon', 'entity_id': _this.coupon.id }
                };
                fileTransfer.upload(_this.image.url, 'http://app.y-travel.net/admin_api/v1/files/' + _this.coupon.id + '?_method=PATCH', options)
                    .then(function (data) {
                    loading.dismiss();
                    _this.getCoupons();
                }, function (err) {
                    console.log(err);
                    loading.dismiss();
                    var alert = _this.alertCtrl.create({ title: 'Error', buttons: ['OK'] });
                    alert.present();
                });
            }
            else {
                loading.dismiss();
                _this.getCoupons();
            }
        });
    };
    EditCouponPage.prototype.getCoupons = function () {
        var _this = this;
        this.restangular.one('companies', this.auth.company_id).all('coupons').getList().subscribe(function (data) {
            console.log(data);
            _this.viewCtrl.dismiss(data);
        });
    };
    EditCouponPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.restangular.one('coupons', this.navParams.get('coupon_id')).customGET('allTranslations').subscribe(function (data) {
            _this.coupon = data;
        });
    };
    return EditCouponPage;
}());
EditCouponPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-edit-coupon',template:/*ion-inline-start:"G:\github\videotravel_admin\src\pages\edit-coupon\edit-coupon.html"*/'<ion-header>\n\n\n\n    <ion-navbar color="primary" text-center>\n\n        <ion-title>עריכת קופון</ion-title>\n\n        <ion-buttons end>\n\n            <button ion-button (click)="closeModal()">\n\n                סגור\n\n            </button>\n\n        </ion-buttons>\n\n    </ion-navbar>\n\n\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <div class="div90" text-center>\n\n\n\n        <ion-row *ngIf="coupon.image !== \'\'" align-items-center>\n\n\n\n            <ion-col col-4>\n\n                <img [src]="coupon.image" width="80">\n\n            </ion-col>\n\n            <ion-col col-4>\n\n                <button ion-button small color="primary" (click)="deleteImage()">מחק</button>\n\n            </ion-col>\n\n            <ion-col col-4>\n\n                <button ion-button small color="primary" add-picture (targetLogo)="updateImage($event)">עדכן</button>\n\n            </ion-col>\n\n\n\n        </ion-row>\n\n\n\n        <ion-row *ngIf="image.url !== \'\'" align-items-center>\n\n\n\n            <ion-col col-4>\n\n                <img [src]="image.url" width="80">\n\n            </ion-col>\n\n            <ion-col col-4>\n\n                <button ion-button small color="primary" (click)="deleteLocalImage()">מחק</button>\n\n            </ion-col>\n\n            <ion-col col-4>\n\n                <button ion-button small color="primary" add-picture (targetLogo)="addImage($event)">עדכן</button>\n\n            </ion-col>\n\n\n\n        </ion-row>\n\n\n\n        <ion-row *ngIf="coupon.image === \'\' && image.url === \'\'">\n\n\n\n            <button ion-button block color="primary" add-picture (targetLogo)="addImage($event)">הודף תמונה</button>\n\n\n\n        </ion-row>\n\n\n\n        <ion-list no-lines>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input text-center placeholder="כותרת באנגלית" type="text" [(ngModel)]="coupon.title_en"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input text-center placeholder="כותרת בעברית" type="text" [(ngModel)]="coupon.title_he"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input text-center placeholder="מחיר" type="number" [(ngModel)]="coupon.price"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-input text-center placeholder="כמות" type="number" [(ngModel)]="coupon.quantity"></ion-input>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-textarea placeholder="תיאור באנגלית" text-right [(ngModel)]="coupon.description_en"></ion-textarea>\n\n            </ion-item>\n\n\n\n            <ion-item margin-top class="deleteLeftPadding">\n\n                <ion-textarea placeholder="תיאור בעברית" text-right [(ngModel)]="coupon.description_he"></ion-textarea>\n\n            </ion-item>\n\n\n\n        </ion-list>\n\n\n\n        <button class="buttonNoBorder" ion-button outline no-border color="primary" (click)="editCoupon()">\n\n            <img src="assets/images/arrows_left.png" height="17">&nbsp; שלח\n\n        </button>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\videotravel_admin\src\pages\edit-coupon\edit-coupon.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ViewController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_utils_utils__["a" /* UtilsProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_5__providers_init_service_init_service__["a" /* InitServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_6__ionic_native_file_transfer__["a" /* FileTransfer */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
], EditCouponPage);

//# sourceMappingURL=edit-coupon.js.map

/***/ }),

/***/ 407:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ScanPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_qr_scanner__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_restangular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ngx_restangular__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ScanPage = (function () {
    function ScanPage(navCtrl, qrScanner, restangular) {
        this.navCtrl = navCtrl;
        this.qrScanner = qrScanner;
        this.restangular = restangular;
        this.modeOn = false;
        this.exists = false;
    }
    ScanPage.prototype.scan = function () {
        var _this = this;
        this.modeOn = true;
        this.qrScanner.prepare()
            .then(function (status) {
            if (status.authorized) {
                var scanSub_1 = _this.qrScanner.scan().subscribe(function (text) {
                    console.log('Scanned', text);
                    _this.modeOn = false;
                    _this.qrScanner.hide();
                    scanSub_1.unsubscribe();
                    _this.restangular.all('qr').customGET('', { code: text }).subscribe(function (data) {
                        _this.exists = true;
                    });
                });
                _this.qrScanner.show();
            }
            else if (status.denied) {
            }
            else {
            }
        })
            .catch(function (e) { return console.log('Error is', e); });
    };
    ScanPage.prototype.ionViewDidLeave = function () {
        this.modeOn = false;
        this.exists = false;
    };
    return ScanPage;
}());
ScanPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-scan',template:/*ion-inline-start:"G:\github\videotravel_admin\src\pages\scan\scan.html"*/'<ion-header>\n\n\n\n    <common-header title="סריקה"></common-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n\n\n    <div class="div90" text-center>\n\n\n\n        <button ion-button block (click)="scan()" *ngIf="!modeOn">סריקה</button>\n\n\n\n        <div style="width: 50%; margin: 10px auto;" *ngIf="exists">\n\n            <img src="assets/images/ok.png">\n\n        </div>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\videotravel_admin\src\pages\scan\scan.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_qr_scanner__["a" /* QRScanner */],
        __WEBPACK_IMPORTED_MODULE_3_ngx_restangular__["Restangular"]])
], ScanPage);

//# sourceMappingURL=scan.js.map

/***/ }),

/***/ 409:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PointsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_init_service_init_service__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var PointsPage = (function () {
    function PointsPage(init, auth, restangular, alertCtrl) {
        this.init = init;
        this.auth = auth;
        this.restangular = restangular;
        this.alertCtrl = alertCtrl;
        this.phone = '';
        this.countries = [];
        this.points = this.init.points;
        this.id = this.auth.id;
        this.country = "IL";
        this.language = 'he';
    }
    PointsPage.prototype.sendPoints = function (quantity) {
        var _this = this;
        console.log(this.phone, this.country);
        if (this.phone === '' || this.country === '' || typeof this.country === 'undefined') {
            var alert_1 = this.alertCtrl.create({ title: 'Please enter all data!', buttons: ['OK'] });
            alert_1.present();
            return;
        }
        this.restangular.all('codes').post({ manager_id: this.id, quantity: quantity, phone: this.phone, country: this.country, language: this.language })
            .subscribe(function (data) {
            var alert = _this.alertCtrl.create({ title: 'Successfully sent to ' + data.phone, buttons: ['OK'] });
            alert.present();
        });
    };
    PointsPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.restangular.all('init/countries').getList().subscribe(function (data) { return _this.countries = data; });
    };
    PointsPage.prototype.ionViewDidLeave = function () {
        this.phone = '';
        this.country = "IL";
    };
    return PointsPage;
}());
PointsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-points',template:/*ion-inline-start:"G:\github\videotravel_admin\src\pages\points\points.html"*/'<ion-header>\n\n\n\n    <common-header title="נקודות"></common-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content>\n\n\n\n    <div class="div90" text-center="">\n\n\n\n        <ion-row margin-top class="grey-row">\n\n            <ion-col col-5>\n\n                <ion-select [(ngModel)]="country" class="points-select">\n\n                    <ion-option *ngFor="let country of countries" [value]="country.code">{{country.name}}</ion-option>\n\n                </ion-select>\n\n            </ion-col>\n\n            <ion-col col-7>\n\n                <ion-input text-center placeholder="טלפון" type="number" [(ngModel)]="phone"></ion-input>\n\n            </ion-col>\n\n        </ion-row>\n\n\n\n        <ion-item margin-top class="deleteLeftPadding" dir="rtl">\n\n            <ion-label color="primary" fixed>שפת עמוד נחיתה</ion-label>\n\n            <ion-select [(ngModel)]="language">\n\n                <ion-option value="he">עברית</ion-option>\n\n                <ion-option value="en">English</ion-option>\n\n            </ion-select>\n\n        </ion-item>\n\n\n\n        <ion-row margin-top *ngFor="let item of points">\n\n            <button ion-button color="primary" block (click)="sendPoints(item.quantity)"><span>נקודות</span>&nbsp;{{item.quantity}}</button>\n\n        </ion-row>\n\n\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\videotravel_admin\src\pages\points\points.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__providers_init_service_init_service__["a" /* InitServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"],
        __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* AlertController */]])
], PointsPage);

//# sourceMappingURL=points.js.map

/***/ }),

/***/ 411:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StatisticsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_init_service_init_service__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var StatisticsPage = (function () {
    function StatisticsPage(navCtrl, restangular, init) {
        this.navCtrl = navCtrl;
        this.restangular = restangular;
        this.init = init;
        this.company_id = this.init.company_id;
    }
    StatisticsPage.prototype.ionViewWillEnter = function () {
        return __awaiter(this, void 0, void 0, function () {
            var data, err_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.restangular.one('companies', this.company_id).customGET('stats').toPromise()];
                    case 1:
                        data = _a.sent();
                        this.codes = data.codes;
                        this.purchases = data.purchases;
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        console.log(err_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return StatisticsPage;
}());
StatisticsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-statistics',template:/*ion-inline-start:"G:\github\videotravel_admin\src\pages\statistics\statistics.html"*/'<ion-header>\n\n\n\n    <common-header title="סטטיסטיקה"></common-header>\n\n\n\n</ion-header>\n\n\n\n<ion-content dir="rtl">\n\n\n\n    <ion-row style="border-bottom: 1px solid lightgrey;">\n\n        <ion-col col-12 text-center>\n\n            <h4>קודים שנתנו</h4>\n\n        </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row text-center>\n\n        <ion-col col-2>כמות נקודות</ion-col>\n\n        <ion-col col-5>מי נתן</ion-col>\n\n        <ion-col col-5>מי קיבל</ion-col>\n\n    </ion-row>\n\n\n\n    <div *ngIf="codes && codes.length" text-center>\n\n        <ion-row *ngFor="let code of codes" class="custom-row" align-items-center>\n\n            <ion-col col-2>{{code.reward}}</ion-col>\n\n            <ion-col col-5 class="small-font">\n\n                <div *ngIf="code.sender">\n\n                    <div>{{code.sender.email}}</div>\n\n                    <div>{{code.sender.name}}</div>\n\n                </div>\n\n            </ion-col>\n\n            <ion-col col-5 class="small-font">\n\n                <div *ngIf="code.recipient">\n\n                    <div>{{code.recipient.name}}</div>\n\n                </div>\n\n            </ion-col>\n\n        </ion-row>\n\n    </div>\n\n\n\n    <ion-row style="border-bottom: 1px solid lightgrey;">\n\n        <ion-col col-12 text-center>\n\n            <h4>קופונים שהשתמשו</h4>\n\n        </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row text-center>\n\n        <ion-col col-2>שם הקופון</ion-col>\n\n        <ion-col col-5>משתמש שקנה</ion-col>\n\n        <ion-col col-3>תוקף</ion-col>\n\n        <ion-col col-2>משומש</ion-col>\n\n    </ion-row>\n\n\n\n    <div *ngIf="purchases && purchases.length" text-center>\n\n        <ion-row *ngFor="let purchase of purchases" class="my-row" align-items-center>\n\n            <ion-col col-2 class="small-font">{{purchase.coupon.title}}</ion-col>\n\n            <ion-col col-5 class="small-font">\n\n                <div *ngIf="purchase.user">\n\n                    <div>{{purchase.user.name}}</div>\n\n                </div>\n\n            </ion-col>\n\n            <ion-col col-3 class="small-font">{{purchase.expiration_date}}</ion-col>\n\n            <ion-col col-2>\n\n                <span *ngIf="purchase.used"><ion-icon name="checkmark" style="color: green"></ion-icon></span>\n\n                <span *ngIf="!purchase.used"><ion-icon name="close" style="color: red"></ion-icon></span>\n\n            </ion-col>\n\n        </ion-row>\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\videotravel_admin\src\pages\statistics\statistics.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"],
        __WEBPACK_IMPORTED_MODULE_3__providers_init_service_init_service__["a" /* InitServiceProvider */]])
], StatisticsPage);

//# sourceMappingURL=statistics.js.map

/***/ }),

/***/ 412:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SettingsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};




var SettingsPage = (function () {
    function SettingsPage(navCtrl, auth, alertCtrl, restangular) {
        this.navCtrl = navCtrl;
        this.auth = auth;
        this.alertCtrl = alertCtrl;
        this.restangular = restangular;
        this.company_id = this.auth.company_id;
    }
    SettingsPage.prototype.ionViewWillEnter = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, err_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.restangular.one('companies', this.company_id).customGET('admin').toPromise()];
                    case 1:
                        _a.company = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _b.sent();
                        console.log(err_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    SettingsPage.prototype.update = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert_1, err_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.restangular.one('companies', this.company_id).customPATCH({
                                facebook_en: this.company.facebook_en,
                                facebook_he: this.company.facebook_he,
                                sms_en: this.company.sms_en,
                                sms_he: this.company.sms_he,
                            }, 'updateAdmin').toPromise()];
                    case 1:
                        _a.sent();
                        alert_1 = this.alertCtrl.create({ title: 'Successfully updated', buttons: ['OK'] });
                        alert_1.present();
                        return [3 /*break*/, 3];
                    case 2:
                        err_2 = _a.sent();
                        console.log(err_2);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return SettingsPage;
}());
SettingsPage = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'page-settings',template:/*ion-inline-start:"G:\github\videotravel_admin\src\pages\settings\settings.html"*/'<ion-header>\n\n\n\n    <common-header title="הגדרות הודעות"></common-header>\n\n\n\n</ion-header>\n\n\n\n\n\n<ion-content>\n\n\n\n    <div class="div90" *ngIf="company">\n\n\n\n        <div margin-top text-center><h3>הודעת פייסבוק</h3></div>\n\n\n\n        <ion-item margin-top class="deleteLeftPadding" dir="rtl">\n\n            <ion-textarea rows="4" text-right placeholder="פייסבוק" [(ngModel)]="company.facebook_he"></ion-textarea>\n\n        </ion-item>\n\n\n\n        <div margin-top dir="rtl" text-right>{{company.facebook_he}} <a>[קישור לוידאו ראשי]</a></div>\n\n\n\n        <ion-item margin-top class="deleteLeftPadding" dir="ltr">\n\n            <ion-textarea rows="4" text-left placeholder="Facebook" [(ngModel)]="company.facebook_en"></ion-textarea>\n\n        </ion-item>\n\n\n\n        <div margin-top dir="ltr" text-left>{{company.facebook_en}} <a>[link to main video]</a></div>\n\n\n\n        <div margin-top text-center><h3>הודעת סמס</h3></div>\n\n\n\n        <ion-item margin-top class="deleteLeftPadding" dir="rtl">\n\n            <ion-textarea rows="4" text-right placeholder="הודעת סמס" [(ngModel)]="company.sms_he"></ion-textarea>\n\n        </ion-item>\n\n\n\n        <div margin-top dir="rtl" text-right>{{company.sms_he}} <a>[קישור להורדת אפליקציה]</a></div>\n\n\n\n        <ion-item margin-top class="deleteLeftPadding" dir="ltr">\n\n            <ion-textarea rows="4" text-left placeholder="SMS message" [(ngModel)]="company.sms_en"></ion-textarea>\n\n        </ion-item>\n\n\n\n        <div margin-top dir="ltr" text-left>{{company.sms_en}} <a>[link to application download]</a></div>\n\n\n\n        <div text-center><button margin-top class="buttonNoBorder" ion-button outline no-border color="primary" (click)="update()">\n\n            <img src="assets/images/arrows_left.png" height="25">&nbsp; <span>עדכן</span>\n\n        </button></div>\n\n    </div>\n\n\n\n</ion-content>\n\n'/*ion-inline-end:"G:\github\videotravel_admin\src\pages\settings\settings.html"*/,
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
        __WEBPACK_IMPORTED_MODULE_3__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2_ngx_restangular__["Restangular"]])
], SettingsPage);

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 416:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(417);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(421);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 421:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export RestangularConfigFactory */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(54);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(458);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_login_login__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_company_profile_company_profile__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_storage__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ngx_restangular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_camera__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_native_media_capture__ = __webpack_require__(413);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__agm_core__ = __webpack_require__(393);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_file_transfer__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_geolocation__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_utils_utils__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__providers_auth_service_auth_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_diagnostic__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__providers_init_service_init_service__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__directives_add_picture_add_picture__ = __webpack_require__(883);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__directives_add_video_add_video__ = __webpack_require__(884);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_native_geocoder__ = __webpack_require__(885);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22_videogular2_core__ = __webpack_require__(886);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22_videogular2_core___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_22_videogular2_core__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_videogular2_controls__ = __webpack_require__(888);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23_videogular2_controls___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_23_videogular2_controls__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_videogular2_overlay_play__ = __webpack_require__(902);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24_videogular2_overlay_play___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_24_videogular2_overlay_play__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25_videogular2_buffering__ = __webpack_require__(905);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25_videogular2_buffering___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_25_videogular2_buffering__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__components_common_header_common_header__ = __webpack_require__(908);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_profile_profile__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_employees_employees__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_coupons_coupons__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_add_coupon_add_coupon__ = __webpack_require__(405);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_edit_coupon_edit_coupon__ = __webpack_require__(406);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_scan_scan__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__ionic_native_qr_scanner__ = __webpack_require__(408);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_points_points__ = __webpack_require__(409);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__ionic_native_screen_orientation__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_statistics_statistics__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_settings_settings__ = __webpack_require__(412);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






































function RestangularConfigFactory(RestangularProvider, App, LoadingController, AlertController, AuthServiceProvider) {
    var loading = null;
    RestangularProvider.addFullRequestInterceptor(function (element, operation, path, url, headers, params) {
        loading = LoadingController.create({ content: 'Please wait...' });
        loading.present();
        params.lang = 'he';
        return { params: params, headers: headers, element: element };
    });
    // RestangularProvider.setBaseUrl('http://vt.test/admin_api/v1');
    RestangularProvider.setBaseUrl('http://app.y-travel.net/admin_api/v1');
    // RestangularProvider.setBaseUrl('http://vt.kartisim.co.il/admin_api/v1');
    // Set initial default header
    RestangularProvider.setDefaultHeaders({ Authorization: "Bearer " + AuthServiceProvider.getToken() });
    // Subscribe to token change
    AuthServiceProvider.token$.subscribe(function (token) { return RestangularProvider.setDefaultHeaders({ Authorization: "Bearer " + token }); });
    RestangularProvider.addResponseInterceptor(function (data, operation, what, url, response) {
        loading.dismiss();
        if (data.data) {
            console.log(url, data.data);
        }
        var extractedData = data.data;
        if (extractedData === null) {
            extractedData = [];
        }
        return extractedData;
    });
    RestangularProvider.addErrorInterceptor(function (response, subject, responseHandler) {
        loading.dismiss();
        console.log(response);
        if (response.error && response.error.message) {
            AlertController.create({ title: response.error.message, buttons: ['OK'] }).present();
            return true;
        }
        if (response.data) {
            var title = response.data.message ? response.data.message : 'Server error';
            var message = '';
            if (response.data.error && response.data.error.message) {
                message += response.data.error.message;
            }
            if (response.data.error && response.data.error.fields) {
                for (var errors in response.data.error.fields) {
                    for (var error in response.data.error.fields[errors]) {
                        message += ' ' + response.data.error.fields[errors][error];
                    }
                }
            }
            AlertController.create({ title: title, message: message, buttons: ['OK'] }).present();
            if (response && response.data && response.data.status && response.data.status == 401) {
                AuthServiceProvider.logout();
                App.getRootNavs()[0].setRoot('LoginPage');
            }
            return true;
        }
        return true;
    });
}
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_5__pages_company_profile_company_profile__["a" /* CompanyProfilePage */],
            __WEBPACK_IMPORTED_MODULE_19__directives_add_picture_add_picture__["a" /* AddPictureDirective */],
            __WEBPACK_IMPORTED_MODULE_20__directives_add_video_add_video__["a" /* AddVideoDirective */],
            __WEBPACK_IMPORTED_MODULE_26__components_common_header_common_header__["a" /* CommonHeaderComponent */],
            __WEBPACK_IMPORTED_MODULE_27__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_employees_employees__["a" /* EmployeesPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_coupons_coupons__["a" /* CouponsPage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_add_coupon_add_coupon__["a" /* AddCouponPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_edit_coupon_edit_coupon__["a" /* EditCouponPage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_scan_scan__["a" /* ScanPage */],
            __WEBPACK_IMPORTED_MODULE_34__pages_points_points__["a" /* PointsPage */],
            __WEBPACK_IMPORTED_MODULE_36__pages_statistics_statistics__["a" /* StatisticsPage */],
            __WEBPACK_IMPORTED_MODULE_37__pages_settings_settings__["a" /* SettingsPage */]
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */]),
            __WEBPACK_IMPORTED_MODULE_8__ionic_storage__["a" /* IonicStorageModule */].forRoot({
                name: "__mydb",
                driverOrder: ["sqlite", "websql", "indexeddb"]
            }),
            __WEBPACK_IMPORTED_MODULE_9_ngx_restangular__["RestangularModule"].forRoot([__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_16__providers_auth_service_auth_service__["a" /* AuthServiceProvider */]], RestangularConfigFactory),
            __WEBPACK_IMPORTED_MODULE_12__agm_core__["a" /* AgmCoreModule */].forRoot({ apiKey: "AIzaSyA4uXPidpv7gDsUIXwS30CMQNs5M-t6DOs", libraries: ["places"] }),
            __WEBPACK_IMPORTED_MODULE_22_videogular2_core__["VgCoreModule"],
            __WEBPACK_IMPORTED_MODULE_23_videogular2_controls__["VgControlsModule"],
            __WEBPACK_IMPORTED_MODULE_24_videogular2_overlay_play__["VgOverlayPlayModule"],
            __WEBPACK_IMPORTED_MODULE_25_videogular2_buffering__["VgBufferingModule"],
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
        entryComponents: [
            __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
            __WEBPACK_IMPORTED_MODULE_4__pages_login_login__["a" /* LoginPage */],
            __WEBPACK_IMPORTED_MODULE_5__pages_company_profile_company_profile__["a" /* CompanyProfilePage */],
            __WEBPACK_IMPORTED_MODULE_27__pages_profile_profile__["a" /* ProfilePage */],
            __WEBPACK_IMPORTED_MODULE_28__pages_employees_employees__["a" /* EmployeesPage */],
            __WEBPACK_IMPORTED_MODULE_29__pages_coupons_coupons__["a" /* CouponsPage */],
            __WEBPACK_IMPORTED_MODULE_30__pages_add_coupon_add_coupon__["a" /* AddCouponPage */],
            __WEBPACK_IMPORTED_MODULE_31__pages_edit_coupon_edit_coupon__["a" /* EditCouponPage */],
            __WEBPACK_IMPORTED_MODULE_32__pages_scan_scan__["a" /* ScanPage */],
            __WEBPACK_IMPORTED_MODULE_34__pages_points_points__["a" /* PointsPage */],
            __WEBPACK_IMPORTED_MODULE_36__pages_statistics_statistics__["a" /* StatisticsPage */],
            __WEBPACK_IMPORTED_MODULE_37__pages_settings_settings__["a" /* SettingsPage */]
        ],
        providers: [
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_native_media_capture__["a" /* MediaCapture */],
            __WEBPACK_IMPORTED_MODULE_15__providers_utils_utils__["a" /* UtilsProvider */],
            __WEBPACK_IMPORTED_MODULE_16__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_file_transfer__["a" /* FileTransfer */],
            __WEBPACK_IMPORTED_MODULE_17__ionic_native_diagnostic__["a" /* Diagnostic */],
            __WEBPACK_IMPORTED_MODULE_14__ionic_native_geolocation__["a" /* Geolocation */],
            __WEBPACK_IMPORTED_MODULE_18__providers_init_service_init_service__["a" /* InitServiceProvider */],
            __WEBPACK_IMPORTED_MODULE_21__ionic_native_native_geocoder__["a" /* NativeGeocoder */],
            __WEBPACK_IMPORTED_MODULE_33__ionic_native_qr_scanner__["a" /* QRScanner */],
            __WEBPACK_IMPORTED_MODULE_35__ionic_native_screen_orientation__["a" /* ScreenOrientation */]
        ]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InitServiceProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__ = __webpack_require__(203);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__auth_service_auth_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_restangular__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ngx_restangular___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_ngx_restangular__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__ = __webpack_require__(392);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__utils_utils__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_ionic_angular__ = __webpack_require__(18);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var InitServiceProvider = (function () {
    function InitServiceProvider(restangular, storage, auth, geolocation, utils, platform) {
        this.restangular = restangular;
        this.storage = storage;
        this.auth = auth;
        this.geolocation = geolocation;
        this.utils = utils;
        this.platform = platform;
        this.company_id = this.auth.company_id;
        this.coordinates = { lat: 0, lng: 0 };
        this.owner = false;
        this.approved = false;
    }
    InitServiceProvider.prototype.onInitApp = function () {
        var _this = this;
        return new Promise(function (resolve) {
            // this.getLocation().subscribe(data => console.log(data));
            if (_this.auth.getToken() !== '') {
                _this.company_id = _this.auth.company_id;
                _this.restangular.one('init', _this.company_id).customGET('').subscribe(function (data) {
                    _this.company_categories = data.company_categories;
                    _this.regions = data.regions;
                    _this.company = _this.restangular.restangularizeElement('', data.company, 'companies');
                    _this.points = data.points;
                    _this.approved = _this.company.approved;
                    _this.managers = _this.company.managers;
                    _this.coupons = _this.company.coupons;
                    _this.countries = data.countries;
                    for (var _i = 0, _a = _this.company.managers; _i < _a.length; _i++) {
                        var manager = _a[_i];
                        if (manager.id === _this.auth.id && manager.owner === 1)
                            _this.owner = true;
                    }
                    resolve(data);
                });
            }
            else {
                resolve();
            }
        });
    };
    return InitServiceProvider;
}());
InitServiceProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ngx_restangular__["Restangular"],
        __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
        __WEBPACK_IMPORTED_MODULE_3__auth_service_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_5__ionic_native_geolocation__["a" /* Geolocation */],
        __WEBPACK_IMPORTED_MODULE_6__utils_utils__["a" /* UtilsProvider */],
        __WEBPACK_IMPORTED_MODULE_7_ionic_angular__["l" /* Platform */]])
], InitServiceProvider);

//# sourceMappingURL=init-service.js.map

/***/ }),

/***/ 458:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_login_login__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_auth_service_auth_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_company_profile_company_profile__ = __webpack_require__(206);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_profile_profile__ = __webpack_require__(402);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_init_service_init_service__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_employees_employees__ = __webpack_require__(403);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_coupons_coupons__ = __webpack_require__(404);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_scan_scan__ = __webpack_require__(407);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_points_points__ = __webpack_require__(409);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_screen_orientation__ = __webpack_require__(410);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_storage__ = __webpack_require__(59);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_statistics_statistics__ = __webpack_require__(411);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_settings_settings__ = __webpack_require__(412);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


















var MyApp = (function () {
    function MyApp(platform, statusBar, splashScreen, auth, utils, init, screen, events, storage) {
        var _this = this;
        this.platform = platform;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.auth = auth;
        this.utils = utils;
        this.init = init;
        this.screen = screen;
        this.events = events;
        this.storage = storage;
        this.initializeApp();
        this.events.subscribe('login:success', function () {
            _this.checkPages();
        });
        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'עמוד אישי', component: __WEBPACK_IMPORTED_MODULE_8__pages_profile_profile__["a" /* ProfilePage */], icon: 'person' },
            { title: 'סריקה', component: __WEBPACK_IMPORTED_MODULE_12__pages_scan_scan__["a" /* ScanPage */], icon: 'qr-scanner' },
        ];
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            if (_this.platform.is('cordova')) {
                _this.screen.lock('portrait');
            }
            _this.auth.initialize().then(function () {
                if (_this.auth.getToken() !== '') {
                    _this.init.onInitApp().then(function () {
                        if (_this.init.owner === true) {
                            _this.pages.push({ title: 'הגדרות הודעות', component: __WEBPACK_IMPORTED_MODULE_17__pages_settings_settings__["a" /* SettingsPage */], icon: 'chatboxes' });
                            _this.pages.push({ title: 'סטטיסטיקה', component: __WEBPACK_IMPORTED_MODULE_16__pages_statistics_statistics__["a" /* StatisticsPage */], icon: 'stats' });
                            _this.pages.push({ title: 'עמוד העסק', component: __WEBPACK_IMPORTED_MODULE_7__pages_company_profile_company_profile__["a" /* CompanyProfilePage */], icon: 'create' });
                            _this.pages.push({ title: 'עובדים', component: __WEBPACK_IMPORTED_MODULE_10__pages_employees_employees__["a" /* EmployeesPage */], icon: 'people' });
                            _this.pages.push({ title: 'קופונים', component: __WEBPACK_IMPORTED_MODULE_11__pages_coupons_coupons__["a" /* CouponsPage */], icon: 'cash' });
                        }
                        _this.storage.get('hotel').then(function (hotel) {
                            if (hotel && hotel == 1) {
                                _this.pages.push({ title: 'נקודות', component: __WEBPACK_IMPORTED_MODULE_13__pages_points_points__["a" /* PointsPage */], icon: 'keypad' });
                            }
                        });
                        _this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_company_profile_company_profile__["a" /* CompanyProfilePage */];
                    });
                }
                else {
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */];
                }
            });
        });
    };
    MyApp.prototype.checkPages = function () {
        var _this = this;
        if (this.init.owner === true) {
            this.pages.push({ title: 'הגדרות הודעות', component: __WEBPACK_IMPORTED_MODULE_17__pages_settings_settings__["a" /* SettingsPage */], icon: 'chatboxes' });
            this.pages.push({ title: 'סטטיסטיקה', component: __WEBPACK_IMPORTED_MODULE_16__pages_statistics_statistics__["a" /* StatisticsPage */], icon: 'stats' });
            this.pages.push({ title: 'עמוד העסק', component: __WEBPACK_IMPORTED_MODULE_7__pages_company_profile_company_profile__["a" /* CompanyProfilePage */], icon: 'create' });
            this.pages.push({ title: 'עובדים', component: __WEBPACK_IMPORTED_MODULE_10__pages_employees_employees__["a" /* EmployeesPage */], icon: 'people' });
            this.pages.push({ title: 'קופונים', component: __WEBPACK_IMPORTED_MODULE_11__pages_coupons_coupons__["a" /* CouponsPage */], icon: 'cash' });
        }
        this.storage.get('hotel').then(function (hotel) {
            if (hotel && hotel == 1) {
                _this.pages.push({ title: 'נקודות', component: __WEBPACK_IMPORTED_MODULE_13__pages_points_points__["a" /* PointsPage */], icon: 'keypad' });
            }
        });
    };
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    MyApp.prototype.logout = function () {
        var _this = this;
        this.storage.clear().then(function () {
            _this.auth.id = 0;
            _this.auth.company_id = 0;
            _this.auth.token = '';
            _this.auth._token.next('');
            _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_5__pages_login_login__["a" /* LoginPage */]);
        });
    };
    return MyApp;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */]),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Nav */])
], MyApp.prototype, "nav", void 0);
MyApp = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"G:\github\videotravel_admin\src\app\app.html"*/'<ion-menu [content]="content" side="right">\n\n  <ion-header>\n\n    <ion-toolbar>\n\n      <ion-title>Menu</ion-title>\n\n    </ion-toolbar>\n\n  </ion-header>\n\n\n\n  <ion-content style="background-color: white;" dir="rtl">\n\n    <ion-list *ngIf="pages && pages.length">\n\n      <button menuClose ion-item *ngFor="let p of pages" (click)="openPage(p)">\n\n          <ion-icon name="{{p.icon}}"></ion-icon> &nbsp; {{p.title}}\n\n      </button>\n\n      <button menuClose ion-item (click)="logout()">\n\n        <ion-icon name="exit"></ion-icon> &nbsp; <span>התנתקות</span>\n\n      </button>\n\n    </ion-list>\n\n  </ion-content>\n\n\n\n</ion-menu>\n\n\n\n<!-- Disable swipe-to-go-back because it\'s poor UX to combine STGB with side menus -->\n\n<ion-nav [root]="rootPage" #content swipeBackEnabled="false"></ion-nav>'/*ion-inline-end:"G:\github\videotravel_admin\src\app\app.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["l" /* Platform */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
        __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
        __WEBPACK_IMPORTED_MODULE_6__providers_auth_service_auth_service__["a" /* AuthServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_4__providers_utils_utils__["a" /* UtilsProvider */],
        __WEBPACK_IMPORTED_MODULE_9__providers_init_service_init_service__["a" /* InitServiceProvider */],
        __WEBPACK_IMPORTED_MODULE_14__ionic_native_screen_orientation__["a" /* ScreenOrientation */],
        __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Events */],
        __WEBPACK_IMPORTED_MODULE_15__ionic_storage__["b" /* Storage */]])
], MyApp);

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilsProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_diagnostic__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__ = __webpack_require__(173);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var UtilsProvider = (function () {
    function UtilsProvider(alertCtrl, diagnostic) {
        this.alertCtrl = alertCtrl;
        this.diagnostic = diagnostic;
        this.progress = 0;
        this._progress = new __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.progress$ = this._progress.asObservable();
        this.uploadProcess = false;
        this._uploadProcess = new __WEBPACK_IMPORTED_MODULE_4_rxjs_BehaviorSubject__["BehaviorSubject"](null);
        this.uploadProcess$ = this._uploadProcess.asObservable();
    }
    UtilsProvider.prototype.isAnyFieldEmpty = function (x) {
        var keys = Object.getOwnPropertyNames(x);
        for (var key in keys) {
            if (x[keys[key]] === '' || typeof x[keys[key]] === 'undefined' || x[keys[key]] === null) {
                var alert_1 = this.alertCtrl.create({ title: 'נא למלא את כל השדות', buttons: ['OK'] });
                alert_1.present();
                return true;
            }
        }
        return false;
    };
    UtilsProvider.prototype.isLocationAvailable = function () {
        var _this = this;
        return new __WEBPACK_IMPORTED_MODULE_3_rxjs_Observable__["Observable"](function (observer) {
            _this.diagnostic.isLocationAuthorized().then(function (isAuthorized) {
                console.log('isLocationAuthorized', isAuthorized);
                if (!isAuthorized) {
                    _this.diagnostic.requestLocationAuthorization().then(function (status) {
                        if (status === "GRANTED") {
                            _this.diagnostic.isLocationEnabled().then(function (isEnabled) {
                                console.log('isLocationEnabled', isEnabled);
                                if (isEnabled) {
                                    observer.next({ status: 'success' });
                                    observer.complete();
                                }
                                else {
                                    observer.next({ status: 'error', message: "Location is disabled" });
                                    observer.complete();
                                }
                            });
                        }
                        else {
                            observer.next({ status: 'error', message: "Location is not authorized" });
                            observer.complete();
                        }
                    });
                }
                else {
                    _this.diagnostic.isLocationEnabled().then(function (isEnabled) {
                        console.log('isLocationEnabled', isEnabled);
                        if (isEnabled) {
                            observer.next({ status: 'success' });
                            observer.complete();
                        }
                        else {
                            observer.next({ status: 'error', message: "Location is disabled" });
                            observer.complete();
                        }
                    });
                }
            });
        });
    };
    UtilsProvider.prototype.setProgress = function (value) {
        this.progress = value;
        this._progress.next(value);
    };
    UtilsProvider.prototype.setUploadProcess = function (value) {
        this.uploadProcess = value;
        this._uploadProcess.next(value);
    };
    return UtilsProvider;
}());
UtilsProvider = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_diagnostic__["a" /* Diagnostic */]])
], UtilsProvider);

//# sourceMappingURL=utils.js.map

/***/ }),

/***/ 883:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddPictureDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(216);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AddPictureDirective = (function () {
    function AddPictureDirective(alertCtrl, camera) {
        this.alertCtrl = alertCtrl;
        this.camera = camera;
        this.targetLogo = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    AddPictureDirective.prototype.onClick = function (event) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'בחר',
            buttons: [
                {
                    text: 'Folder',
                    handler: function () {
                        _this.makeLogo(0);
                    }
                },
                {
                    text: 'Camera',
                    handler: function () {
                        _this.makeLogo(1);
                    }
                }
            ]
        });
        alert.present();
    };
    AddPictureDirective.prototype.makeLogo = function (x) {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            allowEdit: true,
            sourceType: x === 0 ? this.camera.PictureSourceType.PHOTOLIBRARY : this.camera.PictureSourceType.CAMERA,
            targetWidth: 600,
            targetHeight: 600,
        };
        this.camera.getPicture(options).then(function (imageData) {
            console.log(imageData);
            // this.logo = imageData;
            _this.targetLogo.emit({ url: imageData, type: 'image/jpeg' });
        }, function (err) {
            console.log('err', err);
        });
    };
    return AddPictureDirective;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('targetLogo'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
], AddPictureDirective.prototype, "targetLogo", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Event]),
    __metadata("design:returntype", void 0)
], AddPictureDirective.prototype, "onClick", null);
AddPictureDirective = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[add-picture]' // Attribute selector
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */]])
], AddPictureDirective);

//# sourceMappingURL=add-picture.js.map

/***/ }),

/***/ 884:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AddVideoDirective; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_diagnostic__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_media_capture__ = __webpack_require__(413);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AddVideoDirective = (function () {
    function AddVideoDirective(alertCtrl, camera, diagnostic, mediaCapture) {
        this.alertCtrl = alertCtrl;
        this.camera = camera;
        this.diagnostic = diagnostic;
        this.mediaCapture = mediaCapture;
        this.targetVideo = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"]();
    }
    AddVideoDirective.prototype.onClick = function (event) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: 'בחר',
            buttons: [
                {
                    text: 'Folder',
                    handler: function () {
                        _this.selectVideo();
                    }
                },
                {
                    text: 'Camera',
                    handler: function () {
                        _this.checkPermissions();
                    }
                }
            ]
        });
        alert.present();
    };
    AddVideoDirective.prototype.selectVideo = function () {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            mediaType: this.camera.MediaType.VIDEO,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        };
        this.camera.getPicture(options).then(function (imageData) {
            console.log(imageData);
            var construct = { fullPath: '', name: '', type: '' };
            construct.fullPath = imageData;
            construct.name = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15) + '.mp4';
            construct.type = 'video/mp4';
            _this.targetVideo.emit(construct);
        }, function (err) {
            console.log('err', err);
        });
    };
    AddVideoDirective.prototype.checkPermissions = function () {
        var _this = this;
        this.diagnostic.isCameraAuthorized(true).then(function (isAuthorized) {
            if (!isAuthorized) {
                _this.diagnostic.requestCameraAuthorization(true).then(function () { return _this.record(); });
            }
            else {
                _this.record();
            }
        }).catch(function (error) { return console.log('Error in camera authorization', error); });
    };
    AddVideoDirective.prototype.record = function () {
        var _this = this;
        var options = { limit: 1, duration: 30 };
        this.mediaCapture.captureVideo(options).then(function (data) {
            console.log(data);
            _this.targetVideo.emit(data[0]);
        }, function (err) { return console.error(err); });
    };
    return AddVideoDirective;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Output"])('targetVideo'),
    __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["EventEmitter"])
], AddVideoDirective.prototype, "targetVideo", void 0);
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["HostListener"])('click', ['$event']),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Event]),
    __metadata("design:returntype", void 0)
], AddVideoDirective.prototype, "onClick", null);
AddVideoDirective = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Directive"])({
        selector: '[add-video]' // Attribute selector
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
        __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_diagnostic__["a" /* Diagnostic */], __WEBPACK_IMPORTED_MODULE_4__ionic_native_media_capture__["a" /* MediaCapture */]])
], AddVideoDirective);

//# sourceMappingURL=add-video.js.map

/***/ }),

/***/ 908:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CommonHeaderComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__providers_utils_utils__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CommonHeaderComponent = (function () {
    function CommonHeaderComponent(zone, utils) {
        var _this = this;
        this.zone = zone;
        this.utils = utils;
        this.full = 0;
        this.empty = 100;
        this.uploadProcess = false;
        this.utils.progress$.subscribe(function (full) {
            _this.zone.run(function () {
                _this.full = full;
                _this.empty = 100 - full;
            });
        });
        this.utils.uploadProcess$.subscribe(function (value) { return _this.uploadProcess = value; });
    }
    return CommonHeaderComponent;
}());
__decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Input"])(),
    __metadata("design:type", String)
], CommonHeaderComponent.prototype, "title", void 0);
CommonHeaderComponent = __decorate([
    Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
        selector: 'common-header',template:/*ion-inline-start:"G:\github\videotravel_admin\src\components\common-header\common-header.html"*/'<ion-navbar color="primary" text-center>\n\n    <!--<ion-buttons start>-->\n\n        <!--<button ion-button>-->\n\n            <!--<ion-icon name="add-circle"></ion-icon>-->\n\n        <!--</button>-->\n\n    <!--</ion-buttons>-->\n\n    <ion-title>{{title}}</ion-title>\n\n    <ion-buttons end>\n\n        <button ion-button menuToggle right>\n\n            <ion-icon name="menu"></ion-icon>\n\n        </button>\n\n    </ion-buttons>\n\n    <!--<div class="row100">-->\n\n    <div class="row100" *ngIf="uploadProcess">\n\n        <div class="progressBarEmpty" [style.width]="empty + \'%\'"></div>\n\n        <div class="progressBarFull" [style.width]="full + \'%\'"></div>\n\n    </div>\n\n</ion-navbar>'/*ion-inline-end:"G:\github\videotravel_admin\src\components\common-header\common-header.html"*/
    }),
    __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgZone"], __WEBPACK_IMPORTED_MODULE_1__providers_utils_utils__["a" /* UtilsProvider */]])
], CommonHeaderComponent);

//# sourceMappingURL=common-header.js.map

/***/ })

},[416]);
//# sourceMappingURL=main.js.map